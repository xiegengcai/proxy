import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.dialect.mysql.parser.MySqlStatementParser;
import com.proxy.parser.tree.sql.parser.SQLStatementParser;

/**
 * Created by liufish on 16/10/30.
 */
public class TestSQL {

    private static final String sql0 = "SHOW DATABASES";

    private static final String sql1 = "show variables like 'lower_case_table_names'";

    private static final String sql3 = "SHOW VARIABLES LIKE 'lower_case_%'";




    //SHOW TABLES
    // USE cater
    //show create table cater.user
    //select * from cater.user  limit 0,1000
    //show columns from proxy0.user

    public static void main(String [] args){



        String logicSql = "/**abc*/SELECT id,name,department,sex,count(*) from user where id in (select id from depart) and sex in(select id from student) limit 0 ,100";

        SQLStatementParser parser = new MySqlStatementParser(logicSql);
        SQLStatement sqlStatement = parser.parseStatement();

        System.out.print(true);

    }
}
