package com.proxy.parser.result;


import com.proxy.common.constant.SqlType;

/**
 * Created by liufish on 16/11/1.
 */
public class SetNamesResult implements ExtraResult {


    private String charSet;

    @Override
    public SqlType getSqlType(){
        return SqlType.SELECT;
    }

    public String getCharSet() {
        return charSet;
    }

    public void setCharSet(String charSet) {
        this.charSet = charSet;
    }
}
