package com.proxy.parser.result;

import com.proxy.common.constant.SqlType;

/**
 * sql解析过程信息
 *
 * Created by liufish on 17/2/18.
 */
public interface ExtraResult {

    SqlType getSqlType();
}
