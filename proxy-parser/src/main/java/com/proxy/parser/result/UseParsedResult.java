package com.proxy.parser.result;

import com.proxy.common.constant.SqlType;

/**
 * Created by liufish on 16/12/28.
 */
public class UseParsedResult implements ExtraResult {

    private String dataBase;

    @Override
    public SqlType getSqlType(){
        return SqlType.USE_DATA_BASE;
    }

    public String getDataBase() {
        return dataBase;
    }

    public void setDataBase(String dataBase) {
        this.dataBase = dataBase;
    }
}
