package com.proxy.parser.result;


import com.proxy.common.constant.SqlType;

/**
 * Created by liufish on 16/11/1.
 */
public class SetTransactionResult implements ExtraResult {

    private String level;

    @Override
    public SqlType getSqlType(){
        return SqlType.SET_TRANSACTION;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
