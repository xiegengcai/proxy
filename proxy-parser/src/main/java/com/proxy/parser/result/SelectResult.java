package com.proxy.parser.result;


import com.proxy.common.constant.SqlType;

/**
 * Created by liufish on 16/11/1.
 */
public class SelectResult implements ExtraResult {

    @Override
    public SqlType getSqlType(){
        return SqlType.SELECT;
    }

}
