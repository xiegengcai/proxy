package com.proxy.parser.result;

import com.proxy.common.constant.SqlType;

/**
 * Created by liufish on 16/12/28.
 */
public class ShowIndexesResult implements ExtraResult {

    @Override
    public SqlType getSqlType(){
        return SqlType.SHOW_INDEXES;
    }


}
