package com.proxy.parser.result;

import com.proxy.common.constant.SqlType;

/**
 * Created by liufish on 16/12/28.
 */
public class ShowCreateTableResult implements ExtraResult {


    private String proxyDatabase;

    private String actualDatabase;

    @Override
    public SqlType getSqlType(){
        return SqlType.SHOW_CREATE_TABLE;
    }


    public String getProxyDatabase() {
        return proxyDatabase;
    }

    public void setProxyDatabase(String proxyDatabase) {
        this.proxyDatabase = proxyDatabase;
    }

    public String getActualDatabase() {
        return actualDatabase;
    }

    public void setActualDatabase(String actualDatabase) {
        this.actualDatabase = actualDatabase;
    }
}
