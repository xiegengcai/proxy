
package com.proxy.parser.tree.sql.dialect.mysql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public class MysqlDeallocatePrepareStatement extends MySqlStatementImpl {
	
	private SQLName statementName;

	public SQLName getStatementName() {
		return statementName;
	}

	public void setStatementName(SQLName statementName) {
		this.statementName = statementName;
	}
	
	public void accept0(MySqlASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, statementName);
        }
        visitor.endVisit(this);
    }

}
