
package com.proxy.parser.tree.sql.ast.expr;

import com.proxy.parser.tree.sql.ast.SQLExpr;

public interface SQLValuableExpr extends SQLExpr {

    Object getValue();

}
