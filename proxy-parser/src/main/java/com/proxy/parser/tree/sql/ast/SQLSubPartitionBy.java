
package com.proxy.parser.tree.sql.ast;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.statement.SQLAssignItem;

public abstract class SQLSubPartitionBy extends SQLObjectImpl {

    protected SQLExpr                 subPartitionsCount;
    protected boolean                 linear;

    protected List<SQLAssignItem>     options              = new ArrayList<SQLAssignItem>();

    protected List<SQLSubPartition> subPartitionTemplate = new ArrayList<SQLSubPartition>();

    public SQLExpr getSubPartitionsCount() {
        return subPartitionsCount;
    }

    public void setSubPartitionsCount(SQLExpr subPartitionsCount) {
        if (subPartitionsCount != null) {
            subPartitionsCount.setParent(this);
        }

        this.subPartitionsCount = subPartitionsCount;
    }

    public boolean isLinear() {
        return linear;
    }

    public void setLinear(boolean linear) {
        this.linear = linear;
    }

    public List<SQLAssignItem> getOptions() {
        return options;
    }

    public List<SQLSubPartition> getSubPartitionTemplate() {
        return subPartitionTemplate;
    }

}
