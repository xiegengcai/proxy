
package com.proxy.parser.tree.sql.ast.statement;

import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLCommentHint;
import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLStatementImpl;

public class SQLCreateDatabaseStatement extends SQLStatementImpl {

    private SQLName name;

    private String               characterSet;
    private String               collate;

    private List<SQLCommentHint> hints;
    
    protected boolean            ifNotExists = false;

    public SQLCreateDatabaseStatement(){
    }
    
    public SQLCreateDatabaseStatement(String dbType){
        super (dbType);
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, name);
        }
        visitor.endVisit(this);
    }

    public SQLName getName() {
        return name;
    }

    public void setName(SQLName name) {
        this.name = name;
    }

    public String getCharacterSet() {
        return characterSet;
    }

    public void setCharacterSet(String characterSet) {
        this.characterSet = characterSet;
    }

    public String getCollate() {
        return collate;
    }

    public void setCollate(String collate) {
        this.collate = collate;
    }

    public List<SQLCommentHint> getHints() {
        return hints;
    }

    public void setHints(List<SQLCommentHint> hints) {
        this.hints = hints;
    }
    
    public boolean isIfNotExists() {
        return ifNotExists;
    }
    
    public void setIfNotExists(boolean ifNotExists) {
        this.ifNotExists = ifNotExists;
    }

}
