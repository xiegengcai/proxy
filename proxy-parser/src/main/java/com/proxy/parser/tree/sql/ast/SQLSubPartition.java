
package com.proxy.parser.tree.sql.ast;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLSubPartition extends SQLObjectImpl {

    protected SQLName           name;

    protected SQLPartitionValue values;

    public SQLName getName() {
        return name;
    }

    public void setName(SQLName name) {
        if (name != null) {
            name.setParent(this);
        }
        this.name = name;
    }
    
    public SQLPartitionValue getValues() {
        return values;
    }

    public void setValues(SQLPartitionValue values) {
        if (values != null) {
            values.setParent(this);
        }
        this.values = values;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, name);
            acceptChild(visitor, values);
        }
        visitor.endVisit(this);
    }

}
