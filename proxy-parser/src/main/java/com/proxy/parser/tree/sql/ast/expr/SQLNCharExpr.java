
package com.proxy.parser.tree.sql.ast.expr;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLNCharExpr extends SQLTextLiteralExpr {

    public SQLNCharExpr(){

    }

    public SQLNCharExpr(String text){
        super(text);
    }

    public void output(StringBuffer buf) {
        if ((this.text == null) || (this.text.length() == 0)) {
            buf.append("NULL");
            return;
        }

        buf.append("N'");
        buf.append(this.text.replaceAll("'", "''"));
        buf.append("'");
    }

    protected void accept0(SQLASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }
}
