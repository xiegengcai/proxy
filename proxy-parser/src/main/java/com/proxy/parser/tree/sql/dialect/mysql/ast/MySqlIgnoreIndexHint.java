
package com.proxy.parser.tree.sql.dialect.mysql.ast;

import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public class MySqlIgnoreIndexHint extends MySqlIndexHintImpl {

    @Override
    public void accept0(MySqlASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, getIndexList());
        }
        visitor.endVisit(this);
    }

}
