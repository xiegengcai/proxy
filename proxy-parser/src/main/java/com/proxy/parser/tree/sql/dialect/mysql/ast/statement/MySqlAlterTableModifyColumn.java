
package com.proxy.parser.tree.sql.dialect.mysql.ast.statement;

import com.proxy.parser.tree.sql.ast.statement.SQLAlterTableItem;
import com.proxy.parser.tree.sql.ast.statement.SQLColumnDefinition;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.dialect.mysql.ast.MySqlObjectImpl;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public class MySqlAlterTableModifyColumn extends MySqlObjectImpl implements SQLAlterTableItem {

    private SQLColumnDefinition newColumnDefinition;

    private boolean             first;

    private SQLExpr firstColumn;
    private SQLExpr             afterColumn;

    @Override
    public void accept0(MySqlASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, newColumnDefinition);

            acceptChild(visitor, firstColumn);
            acceptChild(visitor, afterColumn);
        }
    }

    public SQLExpr getFirstColumn() {
        return firstColumn;
    }

    public void setFirstColumn(SQLExpr firstColumn) {
        this.firstColumn = firstColumn;
    }

    public SQLExpr getAfterColumn() {
        return afterColumn;
    }

    public void setAfterColumn(SQLExpr afterColumn) {
        this.afterColumn = afterColumn;
    }

    public SQLColumnDefinition getNewColumnDefinition() {
        return newColumnDefinition;
    }

    public void setNewColumnDefinition(SQLColumnDefinition newColumnDefinition) {
        this.newColumnDefinition = newColumnDefinition;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

}
