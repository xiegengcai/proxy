
package com.proxy.parser.tree.sql.dialect.mysql.ast;

import com.proxy.parser.tree.sql.ast.SQLObject;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;


public interface MySqlObject extends SQLObject {
    void accept0(MySqlASTVisitor visitor);
}
