
package com.proxy.parser.tree.sql.visitor.functions;

import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;
import com.proxy.parser.tree.sql.visitor.SQLEvalVisitorUtils;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;

public class Left implements Function {

    public final static Left instance = new Left();

    public Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x) {
        if (x.getParameters().size() != 2) {
            return SQLEvalVisitor.EVAL_ERROR;
        }

        SQLExpr param0 = x.getParameters().get(0);
        SQLExpr param1 = x.getParameters().get(1);
        param0.accept(visitor);
        param1.accept(visitor);

        Object param0Value = param0.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);
        Object param1Value = param1.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);
        if (param0Value == null || param1Value == null) {
            return SQLEvalVisitor.EVAL_ERROR;
        }

        String strValue = param0Value.toString();
        int intValue = SQLEvalVisitorUtils.castToInteger(param1Value);
        
        if (intValue > strValue.length()) {
            return SQLEvalVisitor.EVAL_ERROR;
        }

        String result = strValue.substring(0, intValue);

        return result;
    }
}
