
package com.proxy.parser.tree.wall;

import java.util.Properties;

public abstract class WallProviderStatLoggerAdapter implements WallProviderStatLogger {

    public WallProviderStatLoggerAdapter(){

    }

    @Override
    public void log(WallProviderStatValue statValue) {

    }

    @Override
    public void configFromProperties(Properties properties) {

    }
}
