
package com.proxy.parser.tree.sql.ast.expr;

import com.proxy.parser.tree.sql.SQLUtils;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLCharExpr extends SQLTextLiteralExpr implements SQLValuableExpr{

    public SQLCharExpr(){

    }

    public SQLCharExpr(String text){
        super(text);
    }

    @Override
    public void output(StringBuffer buf) {
        if ((this.text == null) || (this.text.length() == 0)) {
            buf.append("NULL");
        } else {
            buf.append("'");
            buf.append(this.text.replaceAll("'", "''"));
            buf.append("'");
        }
    }

    protected void accept0(SQLASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

    @Override
    public Object getValue() {
        return this.text;
    }
    
    public String toString() {
        return SQLUtils.toSQLString(this);
    }
}
