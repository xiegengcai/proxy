
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLAlterTableEnableConstraint extends SQLObjectImpl implements SQLAlterTableItem {

    private SQLName constraintName;

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.constraintName);
        }
        visitor.endVisit(this);
    }

    public SQLName getConstraintName() {
        return constraintName;
    }

    public void setConstraintName(SQLName constraintName) {
        this.constraintName = constraintName;
    }

}
