
package com.proxy.parser.tree.sql.dialect.mysql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLLimit;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public class MySqlShowWarningsStatement extends MySqlStatementImpl implements MySqlShowStatement {

    private boolean count = false;
    private SQLLimit limit;

    public boolean isCount() {
        return count;
    }

    public void setCount(boolean count) {
        this.count = count;
    }

    public SQLLimit getLimit() {
        return limit;
    }

    public void setLimit(SQLLimit limit) {
        this.limit = limit;
    }

    public void accept0(MySqlASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, limit);
        }
        visitor.endVisit(this);
    }
}
