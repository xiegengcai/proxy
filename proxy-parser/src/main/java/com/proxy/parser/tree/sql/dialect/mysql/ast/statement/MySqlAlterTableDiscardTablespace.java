
package com.proxy.parser.tree.sql.dialect.mysql.ast.statement;

import com.proxy.parser.tree.sql.ast.statement.SQLAlterTableItem;
import com.proxy.parser.tree.sql.dialect.mysql.ast.MySqlObject;
import com.proxy.parser.tree.sql.dialect.mysql.ast.MySqlObjectImpl;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class MySqlAlterTableDiscardTablespace extends MySqlObjectImpl implements SQLAlterTableItem, MySqlObject {

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor instanceof MySqlASTVisitor) {
            accept0((MySqlASTVisitor) visitor);
        } else {
            throw new IllegalArgumentException("not support visitor type : " + visitor.getClass().getName());
        }
    }

    public void accept0(MySqlASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

}
