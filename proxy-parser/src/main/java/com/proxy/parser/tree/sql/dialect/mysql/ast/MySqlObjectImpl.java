
package com.proxy.parser.tree.sql.dialect.mysql.ast;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public abstract class MySqlObjectImpl extends SQLObjectImpl implements MySqlObject {

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor instanceof MySqlASTVisitor) {
            accept0((MySqlASTVisitor) visitor);
        } else {
            throw new IllegalArgumentException("not support visitor type : " + visitor.getClass().getName());
        }
    }

    public abstract void accept0(MySqlASTVisitor visitor);
}
