
package com.proxy.parser.tree.sql.visitor;

public interface ParameterizedVisitor extends PrintableVisitor {

    int getReplaceCount();

    void incrementReplaceCunt();

}
