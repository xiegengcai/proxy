
package com.proxy.parser.tree.sql.dialect.mysql.ast;

import com.proxy.parser.tree.sql.ast.SQLHint;


public interface MySqlHint extends SQLHint, MySqlObject {

}
