
package com.proxy.parser.tree.sql.dialect.mysql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public class MySqlShowCreateDatabaseStatement extends MySqlStatementImpl implements MySqlShowStatement {

    private SQLExpr database;

    public void accept0(MySqlASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, database);
        }
        visitor.endVisit(this);
    }

    public SQLExpr getDatabase() {
        return database;
    }

    public void setDatabase(SQLExpr database) {
        this.database = database;
    }

}
