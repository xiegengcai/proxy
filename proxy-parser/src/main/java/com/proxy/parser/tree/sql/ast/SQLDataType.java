
package com.proxy.parser.tree.sql.ast;

import java.util.List;

public interface SQLDataType extends SQLObject {

    String getName();

    void setName(String name);

    List<SQLExpr> getArguments();
}
