
package com.proxy.parser.tree.sql.visitor.functions;

import com.proxy.parser.tree.sql.visitor.SQLEvalVisitorUtils;
import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;

public class Greatest implements Function {

    public final static Greatest instance = new Greatest();

    public Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x) {
        Object result = null;
        for (SQLExpr item : x.getParameters()) {
            item.accept(visitor);

            Object itemValue = item.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);
            if (result == null) {
                result = itemValue;
            } else {
                if (SQLEvalVisitorUtils.gt(itemValue, result)) {
                    result = itemValue;
                }
            }
        }

        return result;
    }
}
