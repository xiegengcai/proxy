
package com.proxy.parser.tree.sql.ast;

public interface SQLSetQuantifier {


    public final static int ALL         = 1;
    public final static int DISTINCT    = 2;

    public final static int UNIQUE      = 3;
    public final static int DISTINCTROW = 4;

}
