
package com.proxy.parser.tree.sql.ast.statement;

import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLName;

public interface SQLForeignKeyConstraint extends SQLConstraint, SQLTableElement, SQLTableConstraint {

    List<SQLName> getReferencingColumns();

    SQLName getReferencedTableName();

    void setReferencedTableName(SQLName value);

    List<SQLName> getReferencedColumns();
}
