
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLPrimaryKeyImpl extends SQLUnique implements SQLPrimaryKey {

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.getName());
            acceptChild(visitor, this.getColumns());
        }
        visitor.endVisit(this);
    }
}
