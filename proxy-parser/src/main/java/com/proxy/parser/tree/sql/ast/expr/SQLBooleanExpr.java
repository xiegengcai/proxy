
package com.proxy.parser.tree.sql.ast.expr;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.SQLExprImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLBooleanExpr extends SQLExprImpl implements SQLExpr, SQLLiteralExpr {

    private boolean value;

    public SQLBooleanExpr(){

    }

    public SQLBooleanExpr(boolean value){
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        visitor.visit(this);

        visitor.endVisit(this);
    }

    public void output(StringBuffer buf) {
        buf.append("x");
        buf.append(value ? "TRUE" : "FALSE");
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (value ? 1231 : 1237);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        SQLBooleanExpr other = (SQLBooleanExpr) obj;
        if (value != other.value) {
            return false;
        }
        return true;
    }

}
