
package com.proxy.parser.tree.sql.visitor.functions;

import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;

public class BitLength implements Function {

    public final static BitLength instance = new BitLength();

    public Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x) {
        if (x.getParameters().size() != 1) {
            return SQLEvalVisitor.EVAL_ERROR;
        }

        SQLExpr param0 = x.getParameters().get(0);
        param0.accept(visitor);

        Object param0Value = param0.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);
        if (param0Value == null) {
            return SQLEvalVisitor.EVAL_ERROR;
        }

        if (param0Value instanceof String) {
            return ((String) param0Value).getBytes().length * 8;
        }
        return SQLEvalVisitor.EVAL_ERROR;
    }
}
