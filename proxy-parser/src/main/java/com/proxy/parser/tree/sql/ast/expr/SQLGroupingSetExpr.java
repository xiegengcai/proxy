
package com.proxy.parser.tree.sql.ast.expr;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLExprImpl;

public class SQLGroupingSetExpr extends SQLExprImpl {

    private final List<SQLExpr> parameters = new ArrayList<SQLExpr>();

    public List<SQLExpr> getParameters() {
        return parameters;
    }
    
    public void addParameter(SQLExpr parameter) {
        if (parameter != null) {
            parameter.setParent(this);
        }
        this.parameters.add(parameter);
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, parameters);
        }
        visitor.endVisit(this);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((parameters == null) ? 0 : parameters.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SQLGroupingSetExpr)) {
            return false;
        }
        SQLGroupingSetExpr other = (SQLGroupingSetExpr) obj;
        if (parameters == null) {
            if (other.parameters != null) {
                return false;
            }
        } else if (!parameters.equals(other.parameters)) {
            return false;
        }
        return true;
    }

}
