
package com.proxy.parser.tree.sql.parser;


public class EOFParserException extends ParserException {

    public EOFParserException(){
        super("EOF");
    }
}
