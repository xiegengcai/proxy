
package com.proxy.parser.tree.sql.ast;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLCommentHint extends SQLObjectImpl implements SQLHint {

    private String text;

    public SQLCommentHint(){

    }

    public SQLCommentHint(String text){

        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    protected void accept0(SQLASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }
}
