
package com.proxy.parser.tree.sql.ast.expr;

public enum SQLAggregateOption {

    DISTINCT,
    ALL,
    UNIQUE,
    DEDUPLICATION // just for nut

}
