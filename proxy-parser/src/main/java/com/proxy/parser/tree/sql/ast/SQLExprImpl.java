
package com.proxy.parser.tree.sql.ast;

public abstract class SQLExprImpl extends SQLObjectImpl implements SQLExpr {

    public SQLExprImpl(){

    }

    public abstract boolean equals(Object o);

    public abstract int hashCode();
}
