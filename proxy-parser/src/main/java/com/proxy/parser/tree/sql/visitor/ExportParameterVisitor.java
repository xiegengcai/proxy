
package com.proxy.parser.tree.sql.visitor;

import java.util.List;


public interface ExportParameterVisitor extends SQLASTVisitor {
    List<Object> getParameters();
}
