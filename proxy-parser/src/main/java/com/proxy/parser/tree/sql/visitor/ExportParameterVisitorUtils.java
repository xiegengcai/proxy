
package com.proxy.parser.tree.sql.visitor;

import java.util.List;

import com.proxy.parser.tree.sql.ast.expr.SQLBetweenExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLBinaryOpExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLLiteralExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLNumericLiteralExpr;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlExportParameterVisitor;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.util.JdbcUtils;
import com.proxy.parser.tree.sql.ast.expr.SQLBooleanExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLCharExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLVariantRefExpr;

public final class ExportParameterVisitorUtils {
    
    //private for util class not need new instance
    private ExportParameterVisitorUtils() {
        super();
    }

    public static ExportParameterVisitor createExportParameterVisitor(final  Appendable out ,final String dbType) {
        
        if (JdbcUtils.MYSQL.equals(dbType)) {
            return new MySqlExportParameterVisitor(out);
        }

        
        if (JdbcUtils.MARIADB.equals(dbType)) {
            return new MySqlExportParameterVisitor(out);
        }
        
        if (JdbcUtils.H2.equals(dbType)) {
            return new MySqlExportParameterVisitor(out);
        }

       return new ExportParameterizedOutputVisitor(out);
    }

    

    public static boolean exportParamterAndAccept(final List<Object> parameters, List<SQLExpr> list) {
        for (int i = 0, size = list.size(); i < size; ++i) {
            SQLExpr param = list.get(i);

            SQLExpr result = exportParameter(parameters, param);
            if (result != param) {
                list.set(i, result);
            }
        }

        return false;
    }

    public static SQLExpr exportParameter(final List<Object> parameters, final SQLExpr param) {
        if (param instanceof SQLCharExpr) {
            Object value = ((SQLCharExpr) param).getText();
            parameters.add(value);
            return new SQLVariantRefExpr("?");
        }

        if (param instanceof SQLBooleanExpr) {
            Object value = ((SQLBooleanExpr) param).getValue();
            parameters.add(value);
            return new SQLVariantRefExpr("?");
        }

        if (param instanceof SQLNumericLiteralExpr) {
            Object value = ((SQLNumericLiteralExpr) param).getNumber();
            parameters.add(value);
            return new SQLVariantRefExpr("?");
        }

        return param;
    }

    public static void exportParameter(final List<Object> parameters, SQLBinaryOpExpr x) {
        if (x.getLeft() instanceof SQLLiteralExpr && x.getRight() instanceof SQLLiteralExpr && x.getOperator().isRelational()) {
            return;
        }

        {
            SQLExpr leftResult = ExportParameterVisitorUtils.exportParameter(parameters, x.getLeft());
            if (leftResult != x.getLeft()) {
                x.setLeft(leftResult);
            }
        }

        {
            SQLExpr rightResult = exportParameter(parameters, x.getRight());
            if (rightResult != x.getRight()) {
                x.setRight(rightResult);
            }
        }
    }

    public static void exportParameter(final List<Object> parameters, SQLBetweenExpr x) {
        {
            SQLExpr result = exportParameter(parameters, x.getBeginExpr());
            if (result != x.getBeginExpr()) {
                x.setBeginExpr(result);
            }
        }

        {
            SQLExpr result = exportParameter(parameters, x.getEndExpr());
            if (result != x.getBeginExpr()) {
                x.setEndExpr(result);
            }
        }

    }
}
