
package com.proxy.parser.tree.sql.ast.expr;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLExprImpl;

public class SQLAllColumnExpr extends SQLExprImpl {

    public SQLAllColumnExpr(){

    }

    public void output(StringBuffer buf) {
        buf.append("*");
    }

    protected void accept0(SQLASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

    public int hashCode() {
        return 0;
    }

    public boolean equals(Object o) {
        return o instanceof SQLAllColumnExpr;
    }
}
