
package com.proxy.parser.tree.sql.parser;

import com.proxy.parser.tree.sql.ast.statement.SQLTableConstraint;

public class SQLDDLParser extends SQLStatementParser {

    public SQLDDLParser(String sql){
        super(sql);
    }

    public SQLDDLParser(SQLExprParser exprParser){
        super(exprParser);
    }

    protected SQLTableConstraint parseConstraint() {
        if (lexer.token() == Token.CONSTRAINT) {
            lexer.nextToken();
        }

        if (lexer.token() == Token.IDENTIFIER) {
            this.exprParser.name();
            throw new ParserException("TODO");
        }

        if (lexer.token() == Token.PRIMARY) {
            lexer.nextToken();
            accept(Token.KEY);

            throw new ParserException("TODO");
        }

        throw new ParserException("TODO");
    }
}
