
package com.proxy.parser.tree.sql.visitor.functions;

import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;
import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;
import com.proxy.parser.tree.sql.visitor.SQLEvalVisitorUtils;

public class If implements Function {

    public final static If instance = new If();

    public Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x) {
        final List<SQLExpr> parameters = x.getParameters();
        if (parameters.size() == 0) {
            return SQLEvalVisitor.EVAL_ERROR;
        }

        SQLExpr condition = parameters.get(0);
        condition.accept(visitor);
        Object itemValue = condition.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);
        if (itemValue == null) {
            return null;
        }
        if (Boolean.TRUE == itemValue || !SQLEvalVisitorUtils.eq(itemValue, 0)) {
            SQLExpr trueExpr = parameters.get(1);
            trueExpr.accept(visitor);
            return trueExpr.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);
        } else {
            SQLExpr falseExpr = parameters.get(2);
            falseExpr.accept(visitor);
            return falseExpr.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);
        }
    }
}
