
package com.proxy.parser.tree.sql.ast.statement;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLParameter;
import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.ast.SQLStatementImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLBlockStatement extends SQLStatementImpl {
    private String             labelName;

    private List<SQLParameter> parameters    = new ArrayList<SQLParameter>();

    private List<SQLStatement> statementList = new ArrayList<SQLStatement>();

    public List<SQLStatement> getStatementList() {
        return statementList;
    }

    public void setStatementList(List<SQLStatement> statementList) {
        this.statementList = statementList;
    }
    
    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    @Override
    public void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, parameters);
            acceptChild(visitor, statementList);
        }
        visitor.endVisit(this);
    }

    public List<SQLParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<SQLParameter> parameters) {
        this.parameters = parameters;
    }

}
