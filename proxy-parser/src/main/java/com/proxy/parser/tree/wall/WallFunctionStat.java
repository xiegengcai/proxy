
package com.proxy.parser.tree.wall;

import com.proxy.parser.tree.util.JdbcSqlStatUtils;

import static com.proxy.parser.tree.util.JdbcSqlStatUtils.get;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;

public class WallFunctionStat {

    private volatile long                                 invokeCount;
    final static AtomicLongFieldUpdater<WallFunctionStat> invokeCountUpdater = AtomicLongFieldUpdater.newUpdater(WallFunctionStat.class,
                                                                                                                 "invokeCount");

    public long getInvokeCount() {
        return invokeCount;
    }

    public void incrementInvokeCount() {
        invokeCountUpdater.incrementAndGet(this);
    }

    public void addSqlFunctionStat(WallSqlFunctionStat sqlFunctionStat) {
        this.invokeCount += sqlFunctionStat.getInvokeCount();
    }

    public String toString() {
        return "{\"invokeCount\":" + invokeCount + "}";
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("invokeCount", invokeCount);
        return map;
    }

    public WallFunctionStatValue getStatValue(boolean reset) {
        WallFunctionStatValue statValue = new WallFunctionStatValue();
        statValue.setInvokeCount(JdbcSqlStatUtils.get(this, invokeCountUpdater, reset));
        return statValue;
    }
}
