
package com.proxy.parser.tree.sql.dialect.mysql.ast.statement;

import com.proxy.parser.tree.sql.ast.statement.SQLAlterTableItem;
import com.proxy.parser.tree.sql.dialect.mysql.ast.MySqlObjectImpl;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public class MySqlAlterTableOption extends MySqlObjectImpl implements SQLAlterTableItem {

    private String name;
    private Object value;

    public MySqlAlterTableOption(String name, Object value){
        this.name = name;
        this.value = value;
    }

    public MySqlAlterTableOption(){
    }

    @Override
    public void accept0(MySqlASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
