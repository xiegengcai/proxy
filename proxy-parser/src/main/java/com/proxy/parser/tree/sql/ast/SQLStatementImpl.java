
package com.proxy.parser.tree.sql.ast;

import com.proxy.parser.tree.sql.SQLUtils;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public abstract class SQLStatementImpl extends SQLObjectImpl implements SQLStatement {

    private String dbType;

    public SQLStatementImpl(){

    }
    
    public SQLStatementImpl(String dbType){
        this.dbType = dbType;
    }
    
    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String toString() {
        return SQLUtils.toSQLString(this, dbType);
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        throw new UnsupportedOperationException(this.getClass().getName());
    }
}
