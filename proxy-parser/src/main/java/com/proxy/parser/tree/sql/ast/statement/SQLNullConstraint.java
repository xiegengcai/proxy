
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLNullConstraint extends SQLConstraintImpl implements SQLColumnConstraint {

    public SQLNullConstraint(){
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }
}
