
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLAssignItem extends SQLObjectImpl {

    private SQLExpr target;
    private SQLExpr value;

    public SQLAssignItem(){
    }

    public SQLAssignItem(SQLExpr target, SQLExpr value){
        setTarget(target);
        setValue(value);
    }

    public SQLExpr getTarget() {
        return target;
    }

    public void setTarget(SQLExpr target) {
        if (target != null) {
            target.setParent(this);
        }
        this.target = target;
    }

    public SQLExpr getValue() {
        return value;
    }

    public void setValue(SQLExpr value) {
        if (value != null) {
            value.setParent(this);
        }
        this.value = value;
    }

    public void output(StringBuffer buf) {
        target.output(buf);
        buf.append(" = ");
        value.output(buf);
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.target);
            acceptChild(visitor, this.value);
        }
        visitor.endVisit(this);
    }

}
