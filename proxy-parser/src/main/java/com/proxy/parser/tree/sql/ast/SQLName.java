
package com.proxy.parser.tree.sql.ast;

public interface SQLName extends SQLExpr {
    String getSimpleName();
}
