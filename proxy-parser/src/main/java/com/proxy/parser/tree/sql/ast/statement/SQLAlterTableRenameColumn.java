
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLAlterTableRenameColumn extends SQLObjectImpl implements SQLAlterTableItem {

    private SQLName column;
    private SQLName to;

    public SQLAlterTableRenameColumn(){

    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, column);
            acceptChild(visitor, to);
        }
        visitor.endVisit(this);
    }

    public SQLName getColumn() {
        return column;
    }

    public void setColumn(SQLName column) {
        this.column = column;
    }

    public SQLName getTo() {
        return to;
    }

    public void setTo(SQLName to) {
        this.to = to;
    }

}
