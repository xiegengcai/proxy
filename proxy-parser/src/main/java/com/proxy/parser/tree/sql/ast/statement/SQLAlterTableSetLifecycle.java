
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;

public class SQLAlterTableSetLifecycle extends SQLObjectImpl implements SQLAlterTableItem {

    private SQLExpr lifecycle;

    public SQLExpr getLifecycle() {
        return lifecycle;
    }

    public void setLifecycle(SQLExpr comment) {
        if (comment != null) {
            comment.setParent(this);
        }
        this.lifecycle = comment;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, lifecycle);
        }
        visitor.endVisit(this);
    }

}
