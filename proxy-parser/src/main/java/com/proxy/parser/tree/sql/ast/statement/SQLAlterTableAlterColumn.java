

package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;

public class SQLAlterTableAlterColumn extends SQLObjectImpl implements SQLAlterTableItem {

    private SQLColumnDefinition column;

    private boolean             setNotNull;

    private boolean             dropNotNull;

    private SQLExpr setDefault;

    private boolean             dropDefault;

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, column);
            acceptChild(visitor, setDefault);
        }
        visitor.endVisit(this);
    }

    public SQLColumnDefinition getColumn() {
        return column;
    }

    public void setColumn(SQLColumnDefinition column) {
        this.column = column;
    }

    public boolean isSetNotNull() {
        return setNotNull;
    }

    public void setSetNotNull(boolean setNotNull) {
        this.setNotNull = setNotNull;
    }

    public boolean isDropNotNull() {
        return dropNotNull;
    }

    public void setDropNotNull(boolean dropNotNull) {
        this.dropNotNull = dropNotNull;
    }

    public SQLExpr getSetDefault() {
        return setDefault;
    }

    public void setSetDefault(SQLExpr setDefault) {
        this.setDefault = setDefault;
    }

    public boolean isDropDefault() {
        return dropDefault;
    }

    public void setDropDefault(boolean dropDefault) {
        this.dropDefault = dropDefault;
    }

}
