
package com.proxy.parser.tree.sql.dialect.mysql.ast.clause;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;
import com.proxy.parser.tree.sql.dialect.mysql.ast.statement.MySqlStatementImpl;

public class MySqlDeclareHandlerStatement extends MySqlStatementImpl {

	//handler type
	private MySqlHandlerType handleType; 
	//sp statement
	private SQLStatement spStatement;
	
	private List<ConditionValue> conditionValues;
	
	
	public MySqlDeclareHandlerStatement() {
		conditionValues = new ArrayList<ConditionValue>();
	}

	public List<ConditionValue> getConditionValues() {
		return conditionValues;
	}

	public void setConditionValues(List<ConditionValue> conditionValues) {
		this.conditionValues = conditionValues;
	}

	public MySqlHandlerType getHandleType() {
		return handleType;
	}

	public void setHandleType(MySqlHandlerType handleType) {
		this.handleType = handleType;
	}

	public SQLStatement getSpStatement() {
		return spStatement;
	}

	public void setSpStatement(SQLStatement spStatement) {
		this.spStatement = spStatement;
	}

	@Override
	public void accept0(MySqlASTVisitor visitor) {

		 if (visitor.visit(this)) {
	         acceptChild(visitor, spStatement);
	        }
	     visitor.endVisit(this);
		
	}

}

