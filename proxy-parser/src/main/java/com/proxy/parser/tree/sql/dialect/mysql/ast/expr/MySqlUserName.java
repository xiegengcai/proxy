
package com.proxy.parser.tree.sql.dialect.mysql.ast.expr;

import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public class MySqlUserName extends MySqlExprImpl implements SQLName {

    private String userName;
    private String host;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public void accept0(MySqlASTVisitor visitor) {
        visitor.visit(this);
        visitor.endVisit(this);
    }

    public String getSimpleName() {
        return userName + '@' + host;
    }

    public String toString() {
        return getSimpleName();
    }
}
