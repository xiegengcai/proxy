
package com.proxy.parser.tree.sql.dialect.mysql.ast.clause;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;
import com.proxy.parser.tree.sql.dialect.mysql.ast.statement.MySqlStatementImpl;


public class MySqlWhileStatement extends MySqlStatementImpl {

    //while expr
    private SQLExpr condition;
    private List<SQLStatement> statements = new ArrayList<SQLStatement>();
    //while label name
    private String labelName;


    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    @Override
    public void accept0(MySqlASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, condition);
            acceptChild(visitor, statements);
        }
        visitor.endVisit(this);
    }

    public List<SQLStatement> getStatements() {
        return statements;
    }

    public void setStatements(List<SQLStatement> statements) {
        this.statements = statements;
    }

    public SQLExpr getCondition() {
        return condition;
    }

    public void setCondition(SQLExpr condition) {
        this.condition = condition;
    }
}
