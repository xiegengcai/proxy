
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.ast.SQLObject;

public interface SQLConstraint extends SQLObject {

    SQLName getName();

    void setName(SQLName value);
}
