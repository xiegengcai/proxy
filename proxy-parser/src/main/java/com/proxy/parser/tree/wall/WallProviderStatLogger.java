
package com.proxy.parser.tree.wall;

import java.util.Properties;

public interface WallProviderStatLogger {

    void log(WallProviderStatValue statValue);

    void configFromProperties(Properties properties);
}
