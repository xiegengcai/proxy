
package com.proxy.parser.tree.sql.ast.statement;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLCommentHint;
import com.proxy.parser.tree.sql.ast.SQLStatementImpl;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLSetStatement extends SQLStatementImpl {

    private List<SQLAssignItem> items = new ArrayList<SQLAssignItem>();
    
    private List<SQLCommentHint> hints;

    public SQLSetStatement(){
    }
    
    public SQLSetStatement(String dbType){
        super (dbType);
    }
    
    public SQLSetStatement(SQLExpr target, SQLExpr value){
        this(target, value, null);
    }

    public SQLSetStatement(SQLExpr target, SQLExpr value, String dbType){
        super (dbType);
        this.items.add(new SQLAssignItem(target, value));
    }

    public List<SQLAssignItem> getItems() {
        return items;
    }

    public void setItems(List<SQLAssignItem> items) {
        this.items = items;
    }

    public List<SQLCommentHint> getHints() {
        return hints;
    }

    public void setHints(List<SQLCommentHint> hints) {
        this.hints = hints;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.items);
            acceptChild(visitor, this.hints);
        }
        visitor.endVisit(this);
    }

    public void output(StringBuffer buf) {
        buf.append("SET ");

        for (int i = 0; i < items.size(); ++i) {
            if (i != 0) {
                buf.append(", ");
            }

            SQLAssignItem item = items.get(i);
            item.output(buf);
        }
    }
}
