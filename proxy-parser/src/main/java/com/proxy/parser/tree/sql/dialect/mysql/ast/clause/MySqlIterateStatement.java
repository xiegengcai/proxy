
package com.proxy.parser.tree.sql.dialect.mysql.ast.clause;

import com.proxy.parser.tree.sql.dialect.mysql.ast.statement.MySqlStatementImpl;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;


public class MySqlIterateStatement extends MySqlStatementImpl {
	
	private String labelName;
	
	@Override
    public void accept0(MySqlASTVisitor visitor) {
		visitor.visit(this);
        visitor.endVisit(this);
    }

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}
    
}
