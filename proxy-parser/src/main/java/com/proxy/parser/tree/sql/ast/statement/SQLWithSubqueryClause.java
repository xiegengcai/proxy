
package com.proxy.parser.tree.sql.ast.statement;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.ast.expr.SQLIdentifierExpr;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLWithSubqueryClause extends SQLObjectImpl {

    private Boolean           recursive;
    private final List<Entry> entries = new ArrayList<Entry>();

    public List<Entry> getEntries() {
        return entries;
    }
    
    public void addEntry(Entry entrie) {
        if (entrie != null) {
            entrie.setParent(this);
        }
        this.entries.add(entrie);
    }

    public Boolean getRecursive() {
        return recursive;
    }

    public void setRecursive(Boolean recursive) {
        this.recursive = recursive;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, entries);
        }
        visitor.endVisit(this);
    }

    public static class Entry extends SQLObjectImpl {

        protected SQLIdentifierExpr name;
        protected final List<SQLName> columns = new ArrayList<SQLName>();
        protected SQLSelect           subQuery;

        @Override
        protected void accept0(SQLASTVisitor visitor) {
            if (visitor.visit(this)) {
                acceptChild(visitor, name);
                acceptChild(visitor, columns);
                acceptChild(visitor, subQuery);
            }
            visitor.endVisit(this);
        }

        public SQLIdentifierExpr getName() {
            return name;
        }

        public void setName(SQLIdentifierExpr name) {
            this.name = name;
        }

        public SQLSelect getSubQuery() {
            return subQuery;
        }

        public void setSubQuery(SQLSelect subQuery) {
            this.subQuery = subQuery;
        }

        public List<SQLName> getColumns() {
            return columns;
        }

    }
}
