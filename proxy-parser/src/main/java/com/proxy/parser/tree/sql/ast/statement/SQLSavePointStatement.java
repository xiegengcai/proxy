
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.SQLStatementImpl;

public class SQLSavePointStatement extends SQLStatementImpl {

    private SQLExpr name;
    
    public SQLSavePointStatement() {
        
    }
    
    public SQLSavePointStatement(String dbType) {
        super (dbType);
    }

    public SQLExpr getName() {
        return name;
    }

    public void setName(SQLExpr name) {
        this.name = name;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, name);
        }
        visitor.endVisit(this);
    }
}
