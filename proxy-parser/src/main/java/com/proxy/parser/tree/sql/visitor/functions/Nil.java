
package com.proxy.parser.tree.sql.visitor.functions;

import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;
import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;

public class Nil implements Function {

    public final static Nil instance = new Nil();

    @Override
    public Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x) {
        return null;
    }

}
