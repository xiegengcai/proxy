
package com.proxy.parser.tree.sql.ast.statement;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLHint;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;

public abstract class SQLTableSourceImpl extends SQLObjectImpl implements SQLTableSource {

    protected String        alias;

    protected List<SQLHint> hints;

    public SQLTableSourceImpl(){

    }

    public SQLTableSourceImpl(String alias){

        this.alias = alias;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public int getHintsSize() {
        if (hints == null) {
            return 0;
        }

        return hints.size();
    }

    public List<SQLHint> getHints() {
        if (hints == null) {
            hints = new ArrayList<SQLHint>(2);
        }
        return hints;
    }

    public void setHints(List<SQLHint> hints) {
        this.hints = hints;
    }
}
