
package com.proxy.parser.tree.sql.ast.statement;

public interface SQLPrimaryKey extends SQLUniqueConstraint, SQLTableElement {

}
