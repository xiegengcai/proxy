
package com.proxy.parser.tree.wall;

import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLObject;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public interface WallVisitor extends SQLASTVisitor {

    WallConfig getConfig();

    WallProvider getProvider();

    List<Violation> getViolations();

    void addViolation(Violation violation);

    boolean isDenyTable(String name);

    String toSQL(SQLObject obj);

    boolean isSqlModified();

    void setSqlModified(boolean sqlModified);
    
    String getDbType();
    
    boolean isSqlEndOfComment();

    void setSqlEndOfComment(boolean sqlEndOfComment);
}
