
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLObjectImpl;

public class SQLAlterTableDropKey extends SQLObjectImpl implements SQLAlterTableItem {

    private SQLName keyName;

    public SQLName getKeyName() {
        return keyName;
    }

    public void setKeyName(SQLName keyName) {
        this.keyName = keyName;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if(visitor.visit(this)) {
            acceptChild(visitor, keyName);
        }
        visitor.endVisit(this);
    }

}
