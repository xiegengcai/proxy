
package com.proxy.parser.tree.sql.parser;

import com.proxy.parser.tree.sql.dialect.mysql.parser.MySqlLexer;
import com.proxy.parser.tree.sql.dialect.mysql.parser.MySqlExprParser;
import com.proxy.parser.tree.sql.dialect.mysql.parser.MySqlStatementParser;
import com.proxy.parser.tree.util.JdbcUtils;

public class SQLParserUtils {

    public static SQLStatementParser createSQLStatementParser(String sql, String dbType) {


        if (JdbcUtils.MYSQL.equals(dbType)) {
            return new MySqlStatementParser(sql);
        }

        if (JdbcUtils.MARIADB.equals(dbType)) {
            return new MySqlStatementParser(sql);
        }



        if (JdbcUtils.H2.equals(dbType)) {
            return new MySqlStatementParser(sql);
        }


        return new SQLStatementParser(sql, dbType);
    }

    public static SQLExprParser createExprParser(String sql, String dbType) {


        if (JdbcUtils.MYSQL.equals(dbType) || //
            JdbcUtils.MARIADB.equals(dbType) || //
            JdbcUtils.H2.equals(dbType)) {
            return new MySqlExprParser(sql);
        }




        return new SQLExprParser(sql);
    }

    public static Lexer createLexer(String sql, String dbType) {
        if (JdbcUtils.MYSQL.equals(dbType) || //
                JdbcUtils.MARIADB.equals(dbType) || //
                JdbcUtils.H2.equals(dbType)) {
            return new MySqlLexer(sql);
        }
        return new Lexer(sql);
    }
}
