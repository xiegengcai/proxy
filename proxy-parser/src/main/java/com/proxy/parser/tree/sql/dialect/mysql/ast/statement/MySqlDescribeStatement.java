
package com.proxy.parser.tree.sql.dialect.mysql.ast.statement;

import com.proxy.parser.tree.sql.ast.statement.SQLDescribeStatement;
import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class MySqlDescribeStatement extends SQLDescribeStatement implements MySqlStatement {

    private SQLName colName;

    public void accept0(MySqlASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, object);
            acceptChild(visitor, colName);
        }
        visitor.endVisit(this);
    }

    @Override
    public void accept0(SQLASTVisitor visitor) {
        if (visitor instanceof MySqlASTVisitor) {
            accept0((MySqlASTVisitor) visitor);
        } else {
            throw new IllegalArgumentException("not support visitor type : " + visitor.getClass().getName());
        }
    }

    public SQLName getColName() {
        return colName;
    }

    public void setColName(SQLName colName) {
        this.colName = colName;
    }

}
