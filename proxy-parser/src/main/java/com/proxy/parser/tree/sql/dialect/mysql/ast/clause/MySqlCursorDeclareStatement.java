
package com.proxy.parser.tree.sql.dialect.mysql.ast.clause;

import com.proxy.parser.tree.sql.ast.statement.SQLSelectStatement;
import com.proxy.parser.tree.sql.dialect.mysql.ast.statement.MySqlStatementImpl;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;


public class MySqlCursorDeclareStatement extends MySqlStatementImpl {

    //cursor name
    private String cursorName;
    //select statement
    private SQLSelectStatement select;

    public String getCursorName() {
        return cursorName;
    }

    public void setCursorName(String cursorName) {
        this.cursorName = cursorName;
    }

    public SQLSelectStatement getSelect() {
        return select;
    }

    public void setSelect(SQLSelectStatement select) {
        this.select = select;
    }

    @Override
    public void accept0(MySqlASTVisitor visitor) {

        if (visitor.visit(this)) {
            acceptChild(visitor, select);
        }
        visitor.endVisit(this);

    }

}
