

package com.proxy.parser.tree.sql.ast.expr;

import com.proxy.parser.tree.sql.ast.SQLExprImpl;

public abstract class SQLNumericLiteralExpr extends SQLExprImpl implements SQLLiteralExpr {

    public SQLNumericLiteralExpr(){

    }

    public abstract Number getNumber();

    public abstract void setNumber(Number number);
}
