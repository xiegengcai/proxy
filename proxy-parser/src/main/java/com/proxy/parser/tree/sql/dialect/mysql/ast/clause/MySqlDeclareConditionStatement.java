
package com.proxy.parser.tree.sql.dialect.mysql.ast.clause;


import com.proxy.parser.tree.sql.dialect.mysql.ast.statement.MySqlStatementImpl;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlASTVisitor;

public class MySqlDeclareConditionStatement extends MySqlStatementImpl {
	

	
	//condition_name
	private String conditionName; 
	//sp statement
	private ConditionValue conditionValue;
	
	public String getConditionName() {
		return conditionName;
	}

	public void setConditionName(String conditionName) {
		this.conditionName = conditionName;
	}

	public ConditionValue getConditionValue() {
		return conditionValue;
	}

	public void setConditionValue(ConditionValue conditionValue) {
		this.conditionValue = conditionValue;
	}

	@Override
	public void accept0(MySqlASTVisitor visitor) {

		visitor.visit(this);
	    visitor.endVisit(this);
		
	}

}

