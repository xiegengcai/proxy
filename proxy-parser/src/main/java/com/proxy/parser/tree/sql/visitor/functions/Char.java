
package com.proxy.parser.tree.sql.visitor.functions;

import java.math.BigDecimal;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;
import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;

public class Char implements Function {

    public final static Char instance = new Char();

    public Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x) {
        if (x.getParameters().size() == 0) {
            return SQLEvalVisitor.EVAL_ERROR;
        }

        StringBuffer buf = new StringBuffer(x.getParameters().size());
        for (SQLExpr param : x.getParameters()) {
            param.accept(visitor);

            Object paramValue = param.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);

            if (paramValue instanceof Number) {
                int charCode = ((Number) paramValue).intValue();
                buf.append((char) charCode);
            } else if (paramValue instanceof String) {
                try {
                    int charCode = new BigDecimal((String) paramValue).intValue();
                    buf.append((char) charCode);
                } catch (NumberFormatException e) {
                }
            } else {
                return SQLEvalVisitor.EVAL_ERROR;
            }
        }

        return buf.toString();
    }
}
