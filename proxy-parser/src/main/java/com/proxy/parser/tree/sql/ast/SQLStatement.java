
package com.proxy.parser.tree.sql.ast;

public interface SQLStatement extends SQLObject {
    String getDbType();
}
