
package com.proxy.parser.tree.sql.ast;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLParameter extends SQLObjectImpl {

    private SQLExpr       name;
    private SQLDataType   dataType;
    private SQLExpr       defaultValue;
    private ParameterType paramType;

    public SQLExpr getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(SQLExpr deaultValue) {
        this.defaultValue = deaultValue;
    }

    public SQLExpr getName() {
        return name;
    }

    public void setName(SQLExpr name) {
        this.name = name;
    }

    public SQLDataType getDataType() {
        return dataType;
    }

    public void setDataType(SQLDataType dataType) {
        this.dataType = dataType;
    }
    
    public ParameterType getParamType() {
        return paramType;
    }

    public void setParamType(ParameterType paramType) {
        this.paramType = paramType;
    }

    @Override
    public void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, name);
            acceptChild(visitor, dataType);
            acceptChild(visitor, defaultValue);
        }
        visitor.endVisit(this);
    }
    
    public static enum ParameterType {
        DEFAULT, //
        IN, // in
        OUT, // out
        INOUT// inout
    }
}
