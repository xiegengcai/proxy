
package com.proxy.parser.tree.sql.dialect.mysql.ast.expr;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.dialect.mysql.ast.MySqlObjectImpl;

public abstract class MySqlExprImpl extends MySqlObjectImpl implements SQLExpr {

}
