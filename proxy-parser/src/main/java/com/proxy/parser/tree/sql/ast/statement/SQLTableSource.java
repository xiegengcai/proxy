
package com.proxy.parser.tree.sql.ast.statement;

import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLHint;
import com.proxy.parser.tree.sql.ast.SQLObject;

public interface SQLTableSource extends SQLObject {

    String getAlias();

    void setAlias(String alias);
    
    List<SQLHint> getHints();
}
