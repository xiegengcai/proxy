
package com.proxy.parser.tree.wall.violation;

import com.proxy.parser.tree.wall.Violation;

public class IllegalSQLObjectViolation implements Violation {

    private final String message;
    private final String sqlPart;
    private final int errorCode;

    public IllegalSQLObjectViolation(int errorCode, String message, String sqlPart){
        this.errorCode = errorCode;
        this.message = message;
        this.sqlPart = sqlPart;
    }

    public String getSqlPart() {
        return sqlPart;
    }

    public String toString() {
        return this.sqlPart;
    }

    
    public String getMessage() {
        return message;
    }

    @Override
    public int getErrorCode() {
        return errorCode;
    }

    
}
