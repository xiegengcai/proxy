
package com.proxy.parser.tree.wall;

import java.util.LinkedHashMap;
import java.util.Map;


public class WallFunctionStatValue {


    private String name;

    private long   invokeCount;

    public WallFunctionStatValue(){

    }

    public WallFunctionStatValue(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getInvokeCount() {
        return invokeCount;
    }

    public void setInvokeCount(long invokeCount) {
        this.invokeCount = invokeCount;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new LinkedHashMap<String, Object>(2);
        map.put("name", name);
        map.put("invokeCount", invokeCount);
        return map;
    }
}
