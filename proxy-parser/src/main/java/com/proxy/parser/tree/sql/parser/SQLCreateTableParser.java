
package com.proxy.parser.tree.sql.parser;

import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.ast.statement.SQLColumnDefinition;
import com.proxy.parser.tree.sql.ast.statement.SQLConstraint;
import com.proxy.parser.tree.sql.ast.statement.SQLCreateTableStatement;
import com.proxy.parser.tree.sql.ast.statement.SQLTableElement;
import com.proxy.parser.tree.sql.ast.statement.SQLExprTableSource;

public class SQLCreateTableParser extends SQLDDLParser {

    public SQLCreateTableParser(String sql){
        super(sql);
    }

    public SQLCreateTableParser(SQLExprParser exprParser){
        super(exprParser);
    }

    public SQLCreateTableStatement parseCrateTable() {
        List<String> comments = null;
        if (lexer.isKeepComments() && lexer.hasComment()) {
            comments = lexer.readAndResetComments();
        }
        
        SQLCreateTableStatement stmt = parseCrateTable(true);
        if (comments != null) {
            stmt.addBeforeComment(comments);
        }
        
        return stmt;
    }

    public SQLCreateTableStatement parseCrateTable(boolean acceptCreate) {
        if (acceptCreate) {
            accept(Token.CREATE);
        }

        SQLCreateTableStatement createTable = newCreateStatement();

        if (identifierEquals("GLOBAL")) {
            lexer.nextToken();

            if (identifierEquals("TEMPORARY")) {
                lexer.nextToken();
                createTable.setType(SQLCreateTableStatement.Type.GLOBAL_TEMPORARY);
            } else {
                throw new ParserException("syntax error " + lexer.token() + " " + lexer.stringVal());
            }
        } else if (lexer.token() == Token.IDENTIFIER && lexer.stringVal().equalsIgnoreCase("LOCAL")) {
            lexer.nextToken();
            if (lexer.token() == Token.IDENTIFIER && lexer.stringVal().equalsIgnoreCase("TEMPORAY")) {
                lexer.nextToken();
                createTable.setType(SQLCreateTableStatement.Type.LOCAL_TEMPORARY);
            } else {
                throw new ParserException("syntax error");
            }
        }

        accept(Token.TABLE);

        createTable.setName(this.exprParser.name());

        if (lexer.token() == Token.LPAREN) {
            lexer.nextToken();

            for (;;) {
                if (lexer.token() == Token.IDENTIFIER //
                    || lexer.token() == Token.LITERAL_ALIAS) {
                    SQLColumnDefinition column = this.exprParser.parseColumn();
                    createTable.getTableElementList().add(column);
                } else if (lexer.token == Token.PRIMARY //
                           || lexer.token == Token.UNIQUE //
                           || lexer.token == Token.CHECK //
                           || lexer.token == Token.CONSTRAINT) {
                    SQLConstraint constraint = this.exprParser.parseConstaint();
                    constraint.setParent(createTable);
                    createTable.getTableElementList().add((SQLTableElement) constraint);
                } else if (lexer.token() == Token.TABLESPACE) {
                    throw new ParserException("TODO " + lexer.token());
                } else {
                    SQLColumnDefinition column = this.exprParser.parseColumn();
                    createTable.getTableElementList().add(column);
                }

                if (lexer.token() == Token.COMMA) {
                    lexer.nextToken();

                    if (lexer.token() == Token.RPAREN) { // compatible for sql server
                        break;
                    }
                    continue;
                }

                break;
            }

            accept(Token.RPAREN);

            if (identifierEquals("INHERITS")) {
                lexer.nextToken();
                accept(Token.LPAREN);
                SQLName inherits = this.exprParser.name();
                createTable.setInherits(new SQLExprTableSource(inherits));
                accept(Token.RPAREN);
            }
        }

        return createTable;
    }

    protected SQLCreateTableStatement newCreateStatement() {
        return new SQLCreateTableStatement(getDbType());
    }

}
