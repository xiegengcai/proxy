
package com.proxy.parser.tree.sql.ast.statement;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.ast.SQLStatementImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLLoopStatement extends SQLStatementImpl {

    private String             labelName;

    private List<SQLStatement> statements = new ArrayList<SQLStatement>();

    @Override
    public void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, statements);
        }
        visitor.endVisit(this);
    }

    public List<SQLStatement> getStatements() {
        return statements;
    }

    public void setStatements(List<SQLStatement> statements) {
        this.statements = statements;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

}
