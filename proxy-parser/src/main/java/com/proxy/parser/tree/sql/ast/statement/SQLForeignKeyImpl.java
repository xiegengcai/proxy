
package com.proxy.parser.tree.sql.ast.statement;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;
import com.proxy.parser.tree.sql.ast.SQLName;

public class SQLForeignKeyImpl extends SQLConstraintImpl implements SQLForeignKeyConstraint {

    private SQLName referencedTableName;
    private List<SQLName> referencingColumns = new ArrayList<SQLName>();
    private List<SQLName> referencedColumns  = new ArrayList<SQLName>();

    public SQLForeignKeyImpl(){

    }

    @Override
    public List<SQLName> getReferencingColumns() {
        return referencingColumns;
    }

    @Override
    public SQLName getReferencedTableName() {
        return referencedTableName;
    }

    @Override
    public void setReferencedTableName(SQLName value) {
        this.referencedTableName = value;
    }

    @Override
    public List<SQLName> getReferencedColumns() {
        return referencedColumns;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.getName());
            acceptChild(visitor, this.getReferencedTableName());
            acceptChild(visitor, this.getReferencingColumns());
            acceptChild(visitor, this.getReferencedColumns());
        }
        visitor.endVisit(this);        
    }

}
