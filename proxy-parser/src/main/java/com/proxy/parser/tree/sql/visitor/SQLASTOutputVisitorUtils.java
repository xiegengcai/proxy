
package com.proxy.parser.tree.sql.visitor;

import com.proxy.parser.tree.sql.ast.expr.SQLIntegerExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLNumberExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLCharExpr;

public class SQLASTOutputVisitorUtils {

    public static boolean visit(PrintableVisitor visitor, SQLIntegerExpr x) {
        visitor.print(x.getNumber().toString());
        return false;
    }
    
    public static boolean visit(PrintableVisitor visitor, SQLNumberExpr x) {
        visitor.print(x.getNumber().toString());
        return false;
    }
    
    public static boolean visit(PrintableVisitor visitor, SQLCharExpr x) {
        visitor.print('\'');

        String text = x.getText();
        text = text.replaceAll("'", "''");
        text = text.replaceAll("\\\\", "\\\\");

        visitor.print(text);

        visitor.print('\'');
        return false;
    }
}
