
package com.proxy.parser.tree.sql.visitor.functions;

import java.util.Date;

import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;
import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;


public class Now implements Function {
    public final static Now instance = new Now();
    
    public Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x) {
        return new Date();
    }
}
