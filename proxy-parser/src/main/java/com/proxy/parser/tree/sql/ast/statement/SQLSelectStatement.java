
package com.proxy.parser.tree.sql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLStatementImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLSelectStatement extends SQLStatementImpl {

    protected SQLSelect select;

    public SQLSelectStatement(){

    }

    public SQLSelectStatement(String dbType){
        super (dbType);
    }

    public SQLSelectStatement(SQLSelect select){
        this.setSelect(select);
    }

    public SQLSelectStatement(SQLSelect select, String dbType){
        this(dbType);
        this.setSelect(select);
    }

    public SQLSelect getSelect() {
        return this.select;
    }

    public void setSelect(SQLSelect select) {
        if (select != null) {
            select.setParent(this);
        }
        this.select = select;
    }

    public void output(StringBuffer buf) {
        this.select.output(buf);
    }

    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.select);
        }
        visitor.endVisit(this);
    }
}
