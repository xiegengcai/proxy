
package com.proxy.parser.tree.wall;

public interface Violation {

    int getErrorCode();

    String getMessage();

    String toString();
}
