
package com.proxy.parser.tree.sql.ast.statement;

import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLCommentHint;
import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.ast.SQLStatementImpl;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLExplainStatement extends SQLStatementImpl {

    protected SQLStatement statement;

    private List<SQLCommentHint> hints;
    
    public SQLExplainStatement() {
        
    }
    
    public SQLExplainStatement(String dbType) {
        super (dbType);
    }

    public SQLStatement getStatement() {
        return statement;
    }

    public void setStatement(SQLStatement statement) {
        if (statement != null) {
            statement.setParent(this);
        }
        this.statement = statement;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, statement);
        }
        visitor.endVisit(this);
    }

    public List<SQLCommentHint> getHints() {
        return hints;
    }

    public void setHints(List<SQLCommentHint> hints) {
        this.hints = hints;
    }
}
