
package com.proxy.parser.tree.sql.visitor;

import java.util.List;

import com.proxy.parser.tree.sql.visitor.functions.Function;

public interface SQLEvalVisitor extends SQLASTVisitor {

    String EVAL_VALUE       = "eval.value";
    String EVAL_EXPR        = "eval.expr";
    Object EVAL_ERROR       = new Object();
    Object EVAL_VALUE_COUNT = new Object();
    Object EVAL_VALUE_NULL  = new Object();

    Function getFunction(String funcName);

    void registerFunction(String funcName, Function function);

    void unregisterFunction(String funcName);

    List<Object> getParameters();

    void setParameters(List<Object> parameters);

    int incrementAndGetVariantIndex();

    boolean isMarkVariantIndex();

    void setMarkVariantIndex(boolean markVariantIndex);
}
