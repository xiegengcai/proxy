
package com.proxy.parser.tree.sql.visitor.functions;

import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;
import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;


public interface Function {
    Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x);
}
