
package com.proxy.parser.tree.sql.ast;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLSubPartitionByRange extends SQLSubPartitionBy {
    private List<SQLName> columns = new ArrayList<SQLName>();

    public List<SQLName> getColumns() {
        return columns;
    }

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        
    }
}
