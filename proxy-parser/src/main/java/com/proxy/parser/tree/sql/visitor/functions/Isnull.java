
package com.proxy.parser.tree.sql.visitor.functions;

import java.util.List;

import com.proxy.parser.tree.sql.visitor.SQLEvalVisitor;
import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLMethodInvokeExpr;

public class Isnull implements Function {

    public final static Isnull instance = new Isnull();

    public Object eval(SQLEvalVisitor visitor, SQLMethodInvokeExpr x) {
        final List<SQLExpr> parameters = x.getParameters();
        if (parameters.size() == 0) {
            return SQLEvalVisitor.EVAL_ERROR;
        }

        SQLExpr condition = parameters.get(0);
        condition.accept(visitor);
        Object itemValue = condition.getAttributes().get(SQLEvalVisitor.EVAL_VALUE);
        if (itemValue == SQLEvalVisitor.EVAL_VALUE_NULL) {
            return Boolean.TRUE;
        } else if (itemValue == null) {
            return null;
        } else {
            return Boolean.FALSE;
        }
    }
}
