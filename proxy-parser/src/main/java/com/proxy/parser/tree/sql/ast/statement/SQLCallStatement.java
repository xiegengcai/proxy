
package com.proxy.parser.tree.sql.ast.statement;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLExpr;
import com.proxy.parser.tree.sql.ast.SQLName;
import com.proxy.parser.tree.sql.ast.SQLStatementImpl;
import com.proxy.parser.tree.sql.ast.expr.SQLVariantRefExpr;
import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLCallStatement extends SQLStatementImpl {

    private boolean             brace      = false;

    private SQLVariantRefExpr outParameter;

    private SQLName procedureName;

    private final List<SQLExpr> parameters = new ArrayList<SQLExpr>();
    
    public SQLCallStatement() {
        
    }
    
    public SQLCallStatement(String dbType) {
        super (dbType);
    }

    public SQLVariantRefExpr getOutParameter() {
        return outParameter;
    }

    public void setOutParameter(SQLVariantRefExpr outParameter) {
        this.outParameter = outParameter;
    }

    public SQLName getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(SQLName procedureName) {
        this.procedureName = procedureName;
    }

    public List<SQLExpr> getParameters() {
        return parameters;
    }

    public boolean isBrace() {
        return brace;
    }

    public void setBrace(boolean brace) {
        this.brace = brace;
    }

    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, this.outParameter);
            acceptChild(visitor, this.procedureName);
            acceptChild(visitor, this.parameters);
        }
        visitor.endVisit(this);
    }

}
