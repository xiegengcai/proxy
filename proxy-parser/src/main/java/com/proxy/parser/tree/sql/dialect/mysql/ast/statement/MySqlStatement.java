
package com.proxy.parser.tree.sql.dialect.mysql.ast.statement;

import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.dialect.mysql.ast.MySqlObject;

public interface MySqlStatement extends SQLStatement, MySqlObject {

}
