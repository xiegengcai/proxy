
package com.proxy.parser.tree.sql.ast.statement;

import java.util.List;

import com.proxy.parser.tree.sql.ast.SQLExpr;

public interface SQLUniqueConstraint extends SQLConstraint {

    List<SQLExpr> getColumns();

}
