
package com.proxy.parser.tree.sql.ast;

import java.util.ArrayList;
import java.util.List;

import com.proxy.parser.tree.sql.visitor.SQLASTVisitor;

public class SQLPartitionByList extends SQLPartitionBy {

    protected SQLExpr       expr;

    protected List<SQLName> columns = new ArrayList<SQLName>();

    @Override
    protected void accept0(SQLASTVisitor visitor) {
        if (visitor.visit(this)) {
            acceptChild(visitor, expr);
            acceptChild(visitor, columns);
            acceptChild(visitor, partitionsCount);
            acceptChild(visitor, getPartitions());
            acceptChild(visitor, subPartitionBy);
        }
        visitor.endVisit(this);
    }

    public SQLExpr getExpr() {
        return expr;
    }

    public void setExpr(SQLExpr expr) {
        if (expr != null) {
            expr.setParent(this);
        }
        this.expr = expr;
    }

    public List<SQLName> getColumns() {
        return columns;
    }

    public void addColumn(SQLName column) {
        if (column != null) {
            column.setParent(this);
        }
        this.columns.add(column);
    }
}
