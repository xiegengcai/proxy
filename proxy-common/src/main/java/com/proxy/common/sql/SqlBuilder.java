package com.proxy.common.sql;

/**
 * Created by liufish on 17/1/2.
 */
public class SqlBuilder {

    private static SQL  sql = null;

    public static SqlBuilder  builder(){
        sql = new SQL();
        return new SqlBuilder();
    }

    public SqlBuilder UPDATE(String table) {
        sql().UPDATE(table);
        return this;
    }

    public SqlBuilder SET(String sets) {
        sql().SET(sets);
        return this;
    }

    public String SQL() {
        return sql().toString();

    }

    public SqlBuilder INSERT_INTO(String tableName) {
        sql().INSERT_INTO(tableName);
        return this;
    }

    public SqlBuilder VALUES(String columns, String values) {
        sql().VALUES(columns, values);
        return this;
    }

    public SqlBuilder SELECT(String columns) {
        sql().SELECT(columns);
        return this;
    }

    public SqlBuilder SELECT_DISTINCT(String columns) {
        sql().SELECT_DISTINCT(columns);
        return this;
    }

    public SqlBuilder DELETE_FROM(String table) {
        sql().DELETE_FROM(table);
        return this;
    }

    public SqlBuilder FROM(String table) {
        sql().FROM(table);
        return this;
    }

    public SqlBuilder JOIN(String join) {
        sql().JOIN(join);
        return this;
    }

    public SqlBuilder INNER_JOIN(String join) {
        sql().INNER_JOIN(join);
        return this;
    }

    public SqlBuilder LEFT_OUTER_JOIN(String join) {
        sql().LEFT_OUTER_JOIN(join);
        return this;
    }

    public SqlBuilder RIGHT_OUTER_JOIN(String join) {
        sql().RIGHT_OUTER_JOIN(join);
        return this;
    }

    public SqlBuilder OUTER_JOIN(String join) {
        sql().OUTER_JOIN(join);
        return this;
    }

    public SqlBuilder WHERE(String conditions) {
        sql().WHERE(conditions);
        return this;
    }

    public SqlBuilder OR() {
        sql().OR();
        return this;
    }

    public SqlBuilder AND() {
        sql().AND();
        return this;
    }

    public SqlBuilder GROUP_BY(String columns) {
        sql().GROUP_BY(columns);
        return this;
    }

    public SqlBuilder HAVING(String conditions) {
        sql().HAVING(conditions);
        return this;
    }

    public SqlBuilder ORDER_BY(String columns) {
        sql().ORDER_BY(columns);
        return this;
    }

    public SqlBuilder LIMIT(String offset,String rowCount) {
        sql().LIMIT(offset, rowCount);
        return this;
    }

    @Override
    public String toString(){
       return this.sql().toString();
    }

    private SQL sql() {
        return this.sql;
    }



}

