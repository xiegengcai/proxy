package com.proxy.common.exception;

/**
 * Created by liufish on 16/8/11.
 */
public class RouterException extends RuntimeException{


    public RouterException(String message){
        super(message);
    }

    public RouterException(Throwable cause){
        super(cause);
    }

    public RouterException(String message,Throwable cause){
        super(message,cause);
    }

}
