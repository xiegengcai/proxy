package com.proxy.common.model;

import java.util.List;

/**
 * Created by liufish on 16/7/24.
 */
public class Account {

    private String user;

    private String password;


    public Account(String user ,String password ){
        this.user = user ;
        this.password = password ;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
