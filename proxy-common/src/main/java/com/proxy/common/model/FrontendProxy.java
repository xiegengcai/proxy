package com.proxy.common.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by liufish on 16/7/16.
 */
public class FrontendProxy {

    private String schema;

    private int port;

    /**
     * 默认nio
     */
    private boolean bio;

    private byte charsetIndex = 45;


    private List<Account> users;


    public List<Table> tables;


    /**
     * n * cpu
     */
    private int boss = 1;

    /**
     * n * cpu
     */
    private int loop = 3;

    /**
     * n * cpu
     */
    private int event = 8;

    /**
     * 执行线程池。
     */
    private int executor = 256;


    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getBoss() {
        return boss;
    }

    public void setBoss(int boss) {
        this.boss = boss;
    }

    public int getLoop() {
        return loop;
    }

    public void setLoop(int loop) {
        this.loop = loop;
    }

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    public int getExecutor() {
        return executor;
    }

    public void setExecutor(int executor) {
        this.executor = executor;
    }

    public byte getCharsetIndex() {
        return charsetIndex;
    }

    public void setCharsetIndex(byte charsetIndex) {
        this.charsetIndex = charsetIndex;
    }

    public List<Account> getUsers() {
        return users;
    }

    public void setUsers(List<Account> users) {
        this.users = users;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public List<Table> getTables() {
        return tables;
    }

    public void setTables(List<Table> tables) {
        Collections.sort(tables, new Comparator<Table>() {
            @Override
            public int compare(Table table1, Table table2) {
                return table1.getName().compareToIgnoreCase(table2.getName());
            }
        });
        this.tables = tables;
    }

    public boolean isBio() {
        return bio;
    }

    public void setBio(boolean io) {
        this.bio = io;
    }
}
