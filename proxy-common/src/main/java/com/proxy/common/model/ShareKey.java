package com.proxy.common.model;

/**
 * Created by liufish on 16/8/2.
 */
public class ShareKey {

    private String table;

    private String shareColumn;


    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getShareColumn() {
        return shareColumn;
    }

    public void setShareColumn(String shareColumn) {
        this.shareColumn = shareColumn;
    }
}
