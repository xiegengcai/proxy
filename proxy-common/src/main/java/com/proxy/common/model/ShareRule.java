package com.proxy.common.model;

/**
 * Created by liufish on 16/8/3.
 */
public class ShareRule {

    private String id;

    private String name;

    private String method;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
