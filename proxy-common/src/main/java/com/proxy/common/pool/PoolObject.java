package com.proxy.common.pool;


import com.proxy.common.pool.common.PoolObjectState;

/**
 * Created by fish on 17/1/27.
 */
public interface PoolObject<T> {

    T getObject();

    PoolObjectState getState();
}
