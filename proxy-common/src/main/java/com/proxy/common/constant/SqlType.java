package com.proxy.common.constant;

/**
 * Created by liufish on 16/12/24.
 */
public enum SqlType {

    SHOW_AUTHORS(Group.SHOW, 901, "MySqlShowAuthors"),

    SHOW_BINARY_LOGS(Group.SHOW, 902, "MySqlShowBinaryLogs"),

    SHOW_BINARY_LOG_Events(Group.SHOW, 903, "MySqlShowBinLogEvents"),

    SHOW_CHARACTER_SET(Group.SHOW, 904, "MySqlShowCharacterSet"),

    SHOW_COLLATION(Group.SHOW, 905, "MySqlShowCollation"),

    SHOW_COLUMNS(Group.SHOW, 906, "MySqlShowColumns"),

    SHOW_CONTRIBUTORS(Group.SHOW, 907, "MySqlShowContributors"),


    SHOW_DATABASES(Group.SHOW, 908, "MySqlShowDatabases"),

    SHOW_TABLES(Group.SHOW, 909, "SQLShowTables"),

    SHOW_VARIANTS(Group.SHOW, 910, "MySqlShowVariants"),

    SHOW_CREATE_TABLE(Group.SHOW, 911, "MySqlShowCreateTable"),

    SHOW_INDEXES(Group.SHOW, 912, "MySqlShowIndexes"),



    SET_NAMES(Group.SET, 801, "MySqlSetNamesStatement"),

    SET_TRANSACTION(Group.SET, 802, "MySqlSetTransactionStatement"),

    //set autocommit=1



    USE_DATA_BASE(Group.USE,1401,"userStatement"),



    SELECT(Group.SELECT, 701, "SQLSelect"),




    ;


    SqlType(int group, int type, String desc) {

        this.group = group;
        this.type = type;
        this.desc = desc;
    }


    private int group;

    private int type;

    private String desc;


    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public interface Group {

        int BEGIN = 1;

        int COMMIT = 2;

        int DELETE = 3;

        int INSERT = 4;

        int REPLACE = 5;

        int ROLLBACK = 6;

        int SELECT = 7;

        int SET = 8;

        int SHOW = 9;

        int START = 10;

        int UPDATE = 11;

        int KILL = 12;

        int SAVEPOINT = 13;

        int USE = 14;

        int EXPLAIN = 15;

        int KILL_QUERY = 16;
    }


}