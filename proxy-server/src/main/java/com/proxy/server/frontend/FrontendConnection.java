package com.proxy.server.frontend;

import com.proxy.common.model.Account;
import com.proxy.common.utils.CharsetUtil;
import io.netty.channel.Channel;

/**
 * 代理连接
 *
 * Created by liufish on 16/7/15.
 */
public class FrontendConnection {


    /**
     * netty 管道
     */
    private Channel channel;

    /**
     * 是否自动提交
     */
    private volatile boolean autoCommit;

    /**
     * 事物隔离级别
     */
    private volatile int txIsolation;


    /**
     * 事物中断
     */
    private volatile boolean txInterrupted;


    /**
     * 数据库编码控制使用dbCharset来处理
     */
    private String dbCharset = "utf8";

    /**
     * Java相关的字符串编码解码采用charset来表示
     */
    private String charset = "utf-8";

    /**
     * Java相关的字符串编码解码采用charset来表示
     */
    private int charsetIndex = 45;

    /**
     * 用户
     */
    private String user;

    /**
     * 当前数据库
     */
    private String schema;

    /**
     * 密码认证随机值
     */
    private byte[] seed;


    /**
     * 是否已经认证
     */
    private boolean isAuth = false;


    /**
     * 账户信息
     */
    private Account account;



    public FrontendConnection(Channel channel) {
        this.channel = channel;
        this.autoCommit = true;
        this.txInterrupted = false;
    }


    public void close() {
        channel.closeFuture();
    }

    /**
     * 写数据
     */
    public synchronized void write(Object data) {
        channel.writeAndFlush(data);
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    public boolean isTxInterrupted() {
        return txInterrupted;
    }

    public void setTxInterrupted(boolean txInterrupted) {
        this.txInterrupted = txInterrupted;
    }

    public int getTxIsolation() {
        return txIsolation;
    }

    public void setTxIsolation(int txIsolation) {
        this.txIsolation = txIsolation;
    }

    public int getCharsetIndex() {
        return charsetIndex;
    }

    public String getCharset() {
        return charset;
    }

    public boolean setCharsetIndex(int charsetIndex) {
        String charset = CharsetUtil.getDbCharset(charsetIndex);
        if (charset != null) {
            this.dbCharset = charset;
            this.charset = CharsetUtil.getCharset(charsetIndex);
            this.charsetIndex = charsetIndex;
            return true;
        } else {
            return false;
        }
    }

    public boolean setCharset(String charset) {
        int ci = CharsetUtil.getDBIndex(charset);
        if (ci > 0) {
            this.charset = CharsetUtil.getCharset(ci);
            this.dbCharset = charset;
            this.charsetIndex = ci;
            return true;
        } else {
            return false;
        }
    }

    public String getDbCharset() {
        return dbCharset;
    }

    public void setDbCharset(String dbCharset) {
        this.dbCharset = dbCharset;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }

    public Channel getChannel() {
        return channel;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public byte[] getSeed() {
        return seed;
    }

    public void setSeed(byte[] seed) {
        this.seed = seed;
    }

    public boolean isAuth() {
        return isAuth;
    }

    public void setAuth(boolean isAuth) {
        this.isAuth = isAuth;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
