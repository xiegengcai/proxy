package com.proxy.server.frontend.session;


import com.proxy.server.frontend.FrontendConnection;

/**
 * 会话实现类
 *
 * Created by liufish on 16/8/12.
 */
public class ProxySession implements Session {

    private FrontendConnection frontendConnection;

    public ProxySession(FrontendConnection frontendConnection){
        this.frontendConnection = frontendConnection;
    }

    @Override
    public FrontendConnection getFrontendConnection() {
        return this.frontendConnection;
    }

    @Override
    public void execute() {

    }

    @Override
    public void commit() {

    }

    @Override
    public void rollback() {

    }

    @Override
    public void cancel() {

    }

    @Override
    public void close(){

    }




}
