package com.proxy.server.frontend.handler.request.proxy;

import com.proxy.common.packet.BinaryPacket;
import com.proxy.common.packet.OkPacket;
import com.proxy.router.SQLRouteResult;
import com.proxy.server.frontend.session.Session;

/**
 * Created by liufish on 16/7/25.
 */
public class ShowVariantsProxy {

    public static void doProxy(BinaryPacket bin, Session session, SQLRouteResult result){
        session.getFrontendConnection().write(OkPacket.getOk(++bin.packetId));
    }
}
