package com.proxy.server.frontend.session;

import com.proxy.server.frontend.FrontendConnection;

/**
 * 前端会话
 *
 * Created by liufish on 16/12/27.
 */
public interface Session {

    /**
     * 获取前端连接
     */
    FrontendConnection getFrontendConnection();

    /**
     * 开启一个会话执行
     */
    void execute();

    /**
     * 提交一个会话执行
     */
    void commit();

    /**
     * 回滚一个会话执行
     */
    void rollback();

    /**
     * 取消一个正在执行中的会话
     */
    void cancel();

    /**
     * 关闭会话
     */
    void close();
}
