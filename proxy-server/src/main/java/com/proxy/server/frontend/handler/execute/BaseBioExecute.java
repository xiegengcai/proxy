package com.proxy.server.frontend.handler.execute;

/**
 * Created by liufish on 16/12/26.
 */
public abstract class BaseBioExecute {


    public void before(){

    }

    public void checkCharset(){

    }

    void checkTxIsolation(){

    }

    void checkAutocommit(){

    }

    void execute(){

    }

    void after(){

    }

}
