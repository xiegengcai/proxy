package com.proxy.server.frontend.dispatcher;

import com.proxy.common.constant.SqlType;
import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.dialect.mysql.parser.MySqlStatementParser;
import com.proxy.parser.tree.sql.parser.SQLStatementParser;
import com.proxy.common.packet.BinaryPacket;
import com.proxy.router.SQLRouteResult;
import com.proxy.server.frontend.handler.request.proxy.*;
import com.proxy.server.frontend.handler.request.select.SelectRequest;
import com.proxy.server.frontend.handler.request.show.*;
import com.proxy.server.frontend.session.Session;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by liufish on 16/12/21.
 */
public class DispatcherDetail {


    private static final Logger logger = LogManager.getLogger(DispatcherDetail.class);

    /**
     * for show
     */
    public void showStatement(BinaryPacket bin, Session session, SQLRouteResult result) {

        int type = result.getExtra().getSqlType().getType();

        //显示数据库
        if(type == SqlType.SHOW_DATABASES.getType()){
            logger.debug("SHOW DATABASES");
            ShowDatabasesProxy.doProxy(bin,session,result);
            return;
        }

        //显示变量
        if (type == SqlType.SHOW_VARIANTS.getType()) {
            logger.debug("SHOW VARIABLES LIKE 'lower_case_%' ");
            ShowVariantsProxy.doProxy(bin,session,result);
            return;
        }

        //显示数据库表
        if (type == SqlType.SHOW_TABLES.getType()) {
            logger.debug("SQLShowTablesStatement");
            ShowTablesProxy.doProxy(bin,session,result);
            return;

        }

        //显示建表语句
        if (type == SqlType.SHOW_CREATE_TABLE.getType()) {
            logger.debug("MySqlShowCreateTableStatement");
            ShowCreateTableRequest.doResponse(bin, session,result);
        }

      /*  //显示表的字段信息
        if (type == SqlType.SHOW_COLUMNS.getType()) {
            logger.debug("MySqlShowColumnsStatement");
            ShowColumnsRequest.doResponse(bin, session.getFrontendConnection());
        }

        //显示索引
        if (type == SqlType.SHOW_INDEXES.getType()) {
            logger.debug("MySqlShowIndexesStatement");
            ShowIndexesRequest.doResponse(bin, session.getFrontendConnection());
        }
*/
        //其余的走随机路由
        //ShowOthersRequest.dispatch(connection, statement, packetId);


    }


    /**
     * for select
     */
    public void selectStatement(BinaryPacket bin, Session session, SQLRouteResult result) {

        int type = result.getExtra().getSqlType().getType();

        if(type == SqlType.SELECT.getType()){
            SQLStatementParser parser = new MySqlStatementParser(result.getSql());
            SQLStatement sqlStatement = parser.parseStatement();
            //测试NIO,需要修改
            SelectRequest.doResponse(bin,  session.getFrontendConnection(), sqlStatement , result.getSql());
            return;
        }

        //先过滤掉一些无法解析的sql
      /*  if (SelectAtAtRequest.checkAtAt(sql)) {
            SelectAtAtRequest.doResponse(bin, connection, statement, sql);
            return;
        }
        //进行其他查询

        // select * from proxy.user  limit 0,1000
        if (statement instanceof SQLSelectStatement) {
            SelectRequest.doResponse(bin, connection, statement, sql);

        }*/

    }


    /**
     * for set
     */
    public void setStatement(BinaryPacket bin, Session session, SQLRouteResult result) {

        int type = result.getExtra().getSqlType().getType();

        //设置数据库字符集
        if(type == SqlType.SET_NAMES.getType()){
            logger.debug("SET NAMES");
            SetNamesProxy.doProxy(bin,session,result);
            return;
        }


     /*   //set autocommit=1
        if (SetAutocommitRequest.checkAutocommit(sql)) {
            logger.debug("SET autocommitRequest");
            SetAutocommitRequest.doResponse(bin, connection, statement, sql);
            return;
        }
       */

        //设置事物级别
        if (type == SqlType.SET_TRANSACTION.getType()) {
            logger.debug("SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ");
            SetTransactionProxy.doProxy(bin,session,result);
            return;
        }


    }


    /**
     * use
     */
    public void userStatement(BinaryPacket bin, Session session, SQLRouteResult result) {

        int type = result.getExtra().getSqlType().getType();

        //切换数据库
        if(type == SqlType.USE_DATA_BASE.getType()){
            UseDatabaseProxy.doProxy(bin,session,result);
            return;
        }

    }

}
