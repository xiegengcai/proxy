package com.proxy.server.frontend.adapter;

import com.proxy.common.packet.BinaryPacket;
import com.proxy.common.packet.MysqlPacket;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * 前端代理编码器
 * Created by liufish on 16/7/14.
 */
public class FrontendPacketEncoder extends MessageToByteEncoder {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Object o, ByteBuf byteBuf) throws Exception {
        if(o instanceof MysqlPacket){
            MysqlPacket mysqlPacket = (MysqlPacket)o;
            byteBuf.writeBytes(mysqlPacket.writeBuffer(channelHandlerContext.channel().alloc().buffer()));
        }else if (o instanceof ByteBuf){
            byteBuf.writeBytes((ByteBuf)o);
        }else if(o instanceof BinaryPacket){
            BinaryPacket bin = (BinaryPacket)o;
            byteBuf.writeBytes(bin.writeBuffer(channelHandlerContext.channel().alloc().buffer()));
        }else {
            byteBuf.writeBytes((byte[])o);
        }
    }
}
