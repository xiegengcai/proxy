package com.proxy.server.frontend.handler.request.select;

import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.common.constant.ErrorCodeType;
import com.proxy.common.packet.BinaryPacket;
import com.proxy.common.packet.ErrorPacket;
import com.proxy.server.frontend.FrontendConnection;

import java.nio.charset.Charset;

/**
 * Created by liufish on 16/7/25.
 */
public class SelectAtAtRequest {


    final static String atAtSql = "SELECT @@character_set_database, @@collation_database";

    public static boolean checkAtAt(String sql) {
        return sql.equalsIgnoreCase(atAtSql);
    }



    public static void doResponse(BinaryPacket bin, FrontendConnection connection, SQLStatement statement, String sql){
        ErrorPacket error = new ErrorPacket();
        error.packetId =  ++ bin.packetId;
        error.errNo = ErrorCodeType.ER_NOT_SUPPORTED_YET;
        error.message = "not support SELECT @@character_set_database, @@collation_database!".getBytes(Charset.forName(connection.getCharset()));
        connection.write(error);
    }
}
