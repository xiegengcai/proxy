package com.proxy.server.frontend.handler.request.select;

import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.ast.statement.SQLSelectStatement;
import com.proxy.common.constant.PacketType;
import com.proxy.common.packet.BinaryPacket;
import com.proxy.common.packet.CommandPacket;
import com.proxy.server.frontend.session.ProxySession;
import com.proxy.server.backend.BackendConnection;
import com.proxy.server.backend.bio.BioBackendConnection;
import com.proxy.server.backend.nio.NioBackendConnection;
import com.proxy.server.context.AppContext;
import com.proxy.server.frontend.FrontendConnection;
import com.proxy.server.frontend.handler.execute.BioSingleExecute;
import com.proxy.server.frontend.handler.execute.NioSingleExecute;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.UnsupportedEncodingException;

/**
 * Created by liufish on 16/7/31.
 */
public class SelectRequest {

    private static final Logger logger = LogManager.getLogger(SelectRequest.class);

    public static void doResponse(BinaryPacket bin, FrontendConnection connection, SQLStatement statement, String sql){

        //路由确定是否是单个还是多个

        //先测试单个
        BackendConnection backendConnection = null;
        try {
            backendConnection = AppContext.getInstance().getClients().get(0).borrowConnection();

            SQLSelectStatement sqlSelectStatement = (SQLSelectStatement)statement;

            ProxySqlReplaceUtil.replaceSchema(sqlSelectStatement,backendConnection.getSchema());

            byte [] data = null;
            try {
                //backendConnection.getCharset()
                data = sqlSelectStatement.toString().getBytes("utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            CommandPacket packet = new CommandPacket(bin.packetId, PacketType.COM_QUERY,data);

            if(AppContext.getInstance().getFrontendProxy().isBio()){
                BioSingleExecute response = new BioSingleExecute();
                response.execute(connection,(BioBackendConnection)backendConnection,bin,packet);
            }else {
                NioSingleExecute execute = new NioSingleExecute();
                execute.execute(connection,(NioBackendConnection)backendConnection,bin,packet);
            }




        }finally {
            if(backendConnection != null){
                //AppContext.getInstance().getClients().get(0).returnConnection(backendConnection);
            }

        }






    }



    public static void doResponse(BinaryPacket binaryPacket, ProxySession session,String statement){


    }





}
