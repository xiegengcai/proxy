package com.proxy.server.backend.bio;


import com.proxy.common.packet.BinaryPacket;
import com.proxy.server.backend.BackendConnection;

import java.io.OutputStream;


/**
 * Created by liufish on 16/7/25.
 */
public class BioBackendConnection implements BackendConnection {

    private BioBackendClient client;

    private BioChannel channel;


    private String schema;

    /**
     * 是否自动提交
     */
    private volatile boolean autoCommit;

    /**
     * 事物隔离级别
     */
    private volatile int txIsolation;


    /**
     * 事物中断
     */
    private volatile boolean txInterrupted;


    /**
     * 数据库编码控制使用dbCharset来处理
     */
    private String dbCharset;

    /**
     * Java相关的字符串编码解码采用charset来表示
     */
    private String charset;

    /**
     * Java相关的字符串编码解码采用charset来表示
     */
    private int charsetIndex;



    public BioBackendConnection(BioBackendClient client){
        this.client = client;
        this.channel = new BioChannel(client);
        this.schema = this.client.getNode().getSchema();
    }



    public boolean register(){
        return this.channel.connect();
    }


    public BinaryPacket receive(){
        return this.channel.read();
    }

    public void heartbeat(){
        this.channel.heartbeat();
    }


    public boolean isActive(){
        return channel.isActive();
    }


    public void close(){
        this.channel.close();
    }


    public OutputStream getOutputStream(){
        return channel.getOutputStream();
    }


    public int getCharsetIndex() {
        return charsetIndex;
    }

    public void setCharsetIndex(int charsetIndex) {
        this.charsetIndex = charsetIndex;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getDbCharset() {
        return dbCharset;
    }

    public void setDbCharset(String dbCharset) {
        this.dbCharset = dbCharset;
    }

    public boolean isTxInterrupted() {
        return txInterrupted;
    }

    public void setTxInterrupted(boolean txInterrupted) {
        this.txInterrupted = txInterrupted;
    }

    public int getTxIsolation() {
        return txIsolation;
    }

    public void setTxIsolation(int txIsolation) {
        this.txIsolation = txIsolation;
    }

    public boolean isAutoCommit() {
        return autoCommit;
    }

    public void setAutoCommit(boolean autoCommit) {
        this.autoCommit = autoCommit;
    }

    public String getSchema(){
        return this.schema;
    }

    public BioBackendClient getClient(){
        return this.client;
    }
}
