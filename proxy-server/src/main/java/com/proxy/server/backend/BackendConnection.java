package com.proxy.server.backend;

/**
 * 后端连接
 *
 * Created by liufish on 16/12/18.
 */
public interface BackendConnection {

    String getSchema();

    boolean isAutoCommit();

    void setAutoCommit(boolean autoCommit);

    int getTxIsolation();

    void setTxIsolation(int txIsolation);

    boolean isTxInterrupted();

    void setTxInterrupted(boolean txInterrupted);

    String getDbCharset();

    void setDbCharset(String dbCharset);

    String getCharset();

    void setCharset(String charset);

    int getCharsetIndex();

    void setCharsetIndex(int charsetIndex);

    boolean isActive();

}
