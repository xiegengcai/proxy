package com.proxy.server.backend.nio;

import com.proxy.common.exception.ProxyIoException;
import com.proxy.common.model.DataNode;
import com.proxy.common.pool.Pool;
import com.proxy.common.pool.PooledObjectFactory;
import com.proxy.common.pool.common.PoolConfig;
import com.proxy.common.pool.impl.BasePool;
import com.proxy.server.backend.BackendClient;
import com.proxy.server.backend.BackendConnection;
import io.netty.channel.Channel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by liufish on 16/8/2.
 */
public class NioBackendClient implements BackendClient {

    private DataNode node;

    private PooledObjectFactory factory;

    private Pool<BackendConnection> pool;

    public NioBackendClient(DataNode node) {
        this.node = node;
        this._initPool();
    }

    private void _initPool() {
        PoolConfig config = new PoolConfig();
        config.setInitial(node.getInitIdle());
        config.setMaxActive(node.getMaxActive());
        //保证获取的连接是可用的
        config.setTestOnBorrow(true);
        //定时对线程池中空闲的链接进行validateObject校验
        config.setTestWhileIdle(true);
        // 失效检查线程运行时间间隔，如果小于等于0，不会启动检查线程
        config.setTimeBetweenEvictionRunsMillis(node.getTimeBetweenEvictionRunsMillis());
        factory = new NioBackendConnectionFactory(this);
        pool = new BasePool<BackendConnection>(config, factory);

    }


    public DataNode getNode() {
        return this.node;
    }

    @Override
    public BackendConnection borrowConnection() {
        try {
            return pool.borrowObject();
        } catch (Exception e) {
            throw new ProxyIoException(e);
        }
    }

    @Override
    public void returnConnection(BackendConnection connection) {
        pool.returnObject(connection);
        ((NioBackendConnection) connection).getReceiver().clearTrigger();
    }


}
