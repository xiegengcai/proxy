package com.proxy.server.backend;

import com.proxy.common.packet.BinaryPacket;

import java.util.List;

/**
 * Created by liufish on 16/12/21.
 */
public abstract class TriggerBase implements Trigger {


    public void onHeadFieldsEof(BinaryPacket head, List<BinaryPacket> fields, BinaryPacket eof) {
    }

    public void onRow(BinaryPacket row) {
    }

    public void onRowsEof(BinaryPacket bin) {
    }
}
