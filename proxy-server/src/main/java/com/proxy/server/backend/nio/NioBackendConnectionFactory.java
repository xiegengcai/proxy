package com.proxy.server.backend.nio;


import com.proxy.common.pool.PooledObjectFactory;


/**
 * Created by fish on 2016/6/9.
 */
public class NioBackendConnectionFactory implements PooledObjectFactory<NioBackendConnection> {

    private  NioBackendClient client;


    public NioBackendConnectionFactory(NioBackendClient client) {
        this.client = client;
    }


    @Override
    public NioBackendConnection createObject(){
        NioBackendConnection  connection =  new NioBackendConnection(client);
        if(connection.register()){
            return connection;
        }
        return null;
    }

    @Override
    public void destroyObject(NioBackendConnection connection){
        connection.close();
    }


    @Override
    public boolean validateObject(NioBackendConnection connection) {
        return connection.isActive();
    }


    @Override
    public boolean idleObject(NioBackendConnection connection) {
        //设置心跳
        connection.heartbeat();
        return true;
    }
}

