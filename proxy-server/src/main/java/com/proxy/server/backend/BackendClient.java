package com.proxy.server.backend;


/**
 * 后端连接接口
 *
 * Created by liufish on 16/12/19.
 */
public interface BackendClient {

    BackendConnection borrowConnection();

    void returnConnection(BackendConnection connection);
}
