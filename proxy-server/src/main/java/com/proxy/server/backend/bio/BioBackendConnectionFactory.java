package com.proxy.server.backend.bio;


import com.proxy.common.pool.PoolObject;
import com.proxy.common.pool.PooledObjectFactory;
import com.proxy.common.pool.impl.BasePoolObject;

/**
 * Created by fish on 2016/6/9.
 */
public class BioBackendConnectionFactory implements PooledObjectFactory<BioBackendConnection> {

    private BioBackendClient client;

    public BioBackendConnectionFactory(BioBackendClient client) {
        this.client = client;
    }


    @Override
    public BioBackendConnection createObject() {
        BioBackendConnection connection = new BioBackendConnection(client);
        if (connection.register()) {
            return  connection;
        }
        return null;
    }

    @Override
    public void destroyObject(BioBackendConnection object) {
        object.close();
    }

    @Override
    public boolean validateObject(BioBackendConnection object) {
        //根据上一次心跳时间来判断,代码逻辑在方法里面。
        object.heartbeat();
        return object.isActive();
    }


    @Override
    public boolean idleObject(BioBackendConnection object) {
        object.heartbeat();
        return true;
    }
}

