package com.proxy.server.backend;

import com.proxy.common.packet.BinaryPacket;

import java.util.List;

/**
 * 收包触发接口
 *
 * Created by liufish on 16/12/21.
 */
public interface Trigger {

    void onSuccess(BinaryPacket bin);

    void onError(BinaryPacket bin);

    void onHeadFieldsEof(BinaryPacket head, List<BinaryPacket> fields, BinaryPacket eof);

    void onRow(BinaryPacket row);

    void onRowsEof(BinaryPacket bin);
}
