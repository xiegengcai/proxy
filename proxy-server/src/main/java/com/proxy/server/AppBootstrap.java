package com.proxy.server;

import com.proxy.server.context.AppContext;
import com.proxy.server.frontend.FrontendServer;

/**
 * 主程序入口
 *
 * Created by liufish on 16/7/14.
 */
public class AppBootstrap {



    public static void main(String [] args){
        FrontendServer server = null;
        try {
            //启动后端连接
            AppContext.getInstance().init();
            //启动前端代理
            server = new FrontendServer();
            server.start();
            //未完成启动监控
        }catch (Exception ex){
            if(server != null){
                server.shutdown();
            }
            ex.printStackTrace();
        }



    }


}
