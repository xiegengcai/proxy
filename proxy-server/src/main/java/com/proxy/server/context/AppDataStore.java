package com.proxy.server.context;

import com.proxy.server.frontend.session.Session;
import io.netty.channel.Channel;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 这个类打算用来存储监控信息
 *
 * Created by liufish on 16/7/14.
 */
public class AppDataStore {

    public final static Map<Channel, Session> sessionMap = new ConcurrentHashMap<Channel, Session>();

}

