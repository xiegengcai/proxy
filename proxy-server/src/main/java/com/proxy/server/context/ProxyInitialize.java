package com.proxy.server.context;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.proxy.common.model.*;
import com.proxy.common.utils.CharsetUtil;
import com.proxy.common.utils.FileUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 初始化后端接送数据
 *
 * Created by liufish on 16/8/3.
 */
public class ProxyInitialize {

    private String jsonString;

    private FrontendProxy frontendProxy;

    private List<DataNode> nodes = new ArrayList<>();

    public ProxyInitialize(String path){

        this.jsonString = FileUtil.readAsStrByUtf8(path);

        JSONObject jsonObject =  JSON.parseObject(jsonString);
        JSONObject proxy = jsonObject.getJSONObject("proxy");

        int port = proxy.getInteger("port");
        String characterEncoding = proxy.getString("characterEncoding");
        int charsetIndex = CharsetUtil.getIndex(characterEncoding);
        int executor = proxy.getInteger("executor");
        String schema = proxy.getString("schema");
        boolean io = proxy.getBooleanValue("bio");


        frontendProxy = new FrontendProxy();
        frontendProxy.setBio(io);
        frontendProxy.setPort(port);
        frontendProxy.setExecutor(executor);
        frontendProxy.setCharsetIndex((byte) charsetIndex);
        frontendProxy.setSchema(schema);


        List<Table> tableList = new ArrayList<>();
        JSONArray tables = jsonObject.getJSONArray("tables");
        for (int i = 0;i < tables.size() ;i++){
            JSONObject table = tables.getJSONObject(i);
            String name = table.getString("name");
            String shareColumn = table.getString("shareColumn");
            String shareRuleId = table.getString("shareRuleId");
            Table objTable = new Table();
            objTable.setName(name);
            objTable.setShareColumn(shareColumn);
            objTable.setShareRule(shareRuleId);
            tableList.add(objTable);
        }
        frontendProxy.setTables(tableList);

        List<Account> accountList = new ArrayList<>();
        JSONArray users = jsonObject.getJSONArray("users");
        int usersSize = users.size();
        for (int i = 0;i < usersSize ;i++){
            JSONObject user = users.getJSONObject(i);
            String userName = user.getString("username");
            String password = user.getString("password");
            Account account  = new Account(userName,password);
            accountList.add(account);

        }
        frontendProxy.setUsers(accountList);

        //dataSources
        JSONArray dataSources =  jsonObject.getJSONArray("dataSources");
        int size = dataSources.size();
        for(int i = 0;i < size ; i++){
            DataNode node = new DataNode();
            nodes.add(node);
        }
        for (int i = 0;i < size ; i++){
            JSONObject datasource =  dataSources.getJSONObject(i);

            String id = datasource.getString("id");
            int index = datasource.getInteger("index");
            String host = datasource.getString("host");
            int datasourcePort = datasource.getInteger("port");
            String db = datasource.getString("schema");

            String userName = datasource.getString("username");
            String password = datasource.getString("password");


            int connectTimeout = datasource.getInteger("connectTimeout");
            int maxActive = datasource.getInteger("maxActive");
            int initIdle = datasource.getInteger("initIdle");

            //这里要求要按照index的方式来组装存储数据源
            DataNode node = nodes.get(index);
            node.setIndex(index);
            node.setId(id);
            node.setHost(host);
            node.setPort(datasourcePort);
            node.setSchemas(db);
            node.setUser(userName);
            node.setPassword(password);
            node.setMaxActive(maxActive);
            node.setInitIdle(initIdle);
            node.setConnectTimeout(connectTimeout);
        }


    }


    public FrontendProxy getFrontendProxy(){
        if(frontendProxy != null){
            return frontendProxy;
        }
        return null;
    }


    public List<DataNode> getNodes(){
        return nodes;
    }





}
