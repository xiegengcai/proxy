package com.proxy;

import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.dialect.mysql.parser.MySqlStatementParser;

/**
 * Created by liufish on 16/8/15.
 */

public class SqlParseTest {


    public static void main(String [] args){



        String test = "select * from aa.'t_user' t1 left join t_order t2 on t1.id = t2.id and t1 = 'aa.name'";

        String aterSql = _removeSchema(test,"aa");

        System.out.println(aterSql);

    }




    private static String _removeSchema(String sql,String schema){
        final String upSql = sql.toUpperCase();
        final String upSchema = schema.toUpperCase() + ".";
        int strPos = 0;
        int index = 0;
        boolean flag = false;
        index = upSql.indexOf(upSchema, strPos);
        if (index < 0) {
            StringBuilder sb = new StringBuilder("`").append(
                    schema.toUpperCase()).append("`.");
            index = upSql.indexOf(sb.toString(), strPos);
            flag = true;
            if (index < 0) {
                return sql;
            }
        }
        StringBuilder builder = new StringBuilder();
        while (index > 0) {
            builder.append(sql.substring(strPos, index));
            strPos = index + upSchema.length();
            if (flag) {
                strPos += 2;
            }
            index = upSql.indexOf(upSchema, strPos);
        }
        builder.append(sql.substring(strPos));

        return builder.toString();

    }

}
