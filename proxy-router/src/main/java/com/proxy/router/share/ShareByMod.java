package com.proxy.router.share;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 简单取模分片
 *
 * Created by liufish on 16/12/18.
 */
public class ShareByMod {

    public int calculate(boolean value,int dbCount){
        return value ? 1 : 0;
    }

    public int calculate(byte value,int dbCount){
        return value % dbCount;
    }

    public int calculate(int value,int dbCount){
        return value % dbCount;
    }

    public int calculate(BigInteger value, int dbCount){
        return value.remainder(new BigInteger(String.valueOf(dbCount))).intValue();
    }
    public int calculate(float value, int dbCount){
        return (int)(value)% dbCount ;
    }

    public int calculate(double value, int dbCount){
        return new BigDecimal(String.valueOf(value % dbCount)).intValue();
    }

    public int calculate(long value, int dbCount){
        return new BigInteger(String.valueOf(value)).remainder(new BigInteger(String.valueOf(dbCount))).intValue();
    }

    public int calculate(Date value, int dbCount){
        return new BigInteger(String.valueOf(value.getTime())).remainder(new BigInteger(String.valueOf(dbCount))).intValue();
    }

    public int calculate(Time value, int dbCount){
        return new BigInteger(String.valueOf(value.getTime())).remainder(new BigInteger(String.valueOf(dbCount))).intValue();
    }

    public int calculate(Timestamp value, int dbCount){
        return new BigInteger(String.valueOf(value.getTime())).remainder(new BigInteger(String.valueOf(dbCount))).intValue();
    }

    public int calculate(String value, int dbCount) {
       return value.hashCode() % dbCount;
    }


}
