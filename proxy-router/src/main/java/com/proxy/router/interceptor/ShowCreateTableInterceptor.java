package com.proxy.router.interceptor;

import com.proxy.parser.result.ShowCreateTableResult;
import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.ast.expr.SQLIdentifierExpr;
import com.proxy.parser.tree.sql.ast.expr.SQLPropertyExpr;
import com.proxy.parser.tree.sql.dialect.mysql.ast.statement.MySqlShowCreateTableStatement;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.proxy.router.SQLRouteResult;

import java.util.List;

/**
 * Created by liufish on 17/2/18.
 */
public class ShowCreateTableInterceptor extends BaseInterceptor {


    @Override
    public void routing(SQLRouteResult result,
                        SQLStatement sqlStatement,
                        MySqlSchemaStatVisitor visitor,
                        List<Object> parameters){


        MySqlShowCreateTableStatement x = (MySqlShowCreateTableStatement) sqlStatement;

        SQLPropertyExpr expr = (SQLPropertyExpr) x.getName();
        if(expr.getOwner() != null){
            SQLIdentifierExpr owner = (SQLIdentifierExpr)expr.getOwner();
            if(expr.getOwner() != null){
                ShowCreateTableResult showCreateTableResult = new ShowCreateTableResult();
                showCreateTableResult.setProxyDatabase(owner.getName());
                result.setExtra(showCreateTableResult);
            }
        }
    }
}
