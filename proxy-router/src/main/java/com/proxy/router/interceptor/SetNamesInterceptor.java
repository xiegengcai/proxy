package com.proxy.router.interceptor;

import com.proxy.parser.result.SetNamesResult;
import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.dialect.mysql.ast.statement.MySqlSetNamesStatement;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.proxy.router.SQLRouteResult;

import java.util.List;

/**
 * Created by liufish on 17/2/18.
 */
public class SetNamesInterceptor extends BaseInterceptor {

    @Override
    public void routing(SQLRouteResult result,
                        SQLStatement sqlStatement,
                        MySqlSchemaStatVisitor visitor,
                        List<Object> parameters){

        MySqlSetNamesStatement x = (MySqlSetNamesStatement)sqlStatement;

        SetNamesResult setNamesResult = new SetNamesResult();
        setNamesResult.setCharSet(x.getCharSet());

        result.setExtra(setNamesResult);

    }
}
