package com.proxy.router.interceptor;

import com.proxy.parser.result.SetTransactionResult;
import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.dialect.mysql.ast.statement.MySqlSetTransactionStatement;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.proxy.router.SQLRouteResult;

import java.util.List;

/**
 * Created by liufish on 17/2/18.
 */
public class SetTransactionInterceptor extends BaseInterceptor {


    @Override
    public void routing(SQLRouteResult result,
                        SQLStatement sqlStatement,
                        MySqlSchemaStatVisitor visitor,
                        List<Object> parameters){

        MySqlSetTransactionStatement x = (MySqlSetTransactionStatement)sqlStatement;

        SetTransactionResult setTransactionResult = new SetTransactionResult();
        setTransactionResult.setLevel( x.getIsolationLevel());

        result.setExtra(setTransactionResult);
    }
}
