package com.proxy.router.interceptor;

import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.proxy.router.SQLRouteResult;

import java.util.List;

/**
 * Created by liufish on 17/2/18.
 */
public class BaseInterceptor implements Interceptor {

    @Override
    public void before(SQLRouteResult result, List<Object> parameters){

    }


    @Override
    public void routing(SQLRouteResult result,
                        SQLStatement sqlStatement,
                        MySqlSchemaStatVisitor visitor,
                        List<Object> parameters){


    }

    @Override
    public void after(SQLRouteResult result,List<Object> parameters){

    }
}
