package com.proxy.router.interceptor;

import com.proxy.parser.result.UseParsedResult;
import com.proxy.parser.tree.sql.ast.SQLStatement;
import com.proxy.parser.tree.sql.ast.statement.SQLUseStatement;
import com.proxy.parser.tree.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.proxy.router.SQLRouteResult;

import java.util.List;

/**
 * Created by liufish on 17/2/18.
 */
public class SQLUseStatementInterceptor extends BaseInterceptor {


    @Override
    public void routing(SQLRouteResult result,
                        SQLStatement sqlStatement,
                        MySqlSchemaStatVisitor visitor,
                        List<Object> parameters){

        SQLUseStatement x = (SQLUseStatement)sqlStatement;

        UseParsedResult useParsedResult =  new UseParsedResult();

        useParsedResult.setDataBase(x.getDatabase().getSimpleName());

        result.setExtra(useParsedResult);

    }
}
