package com.proxy.router;

import com.proxy.common.model.DataNode;
import com.proxy.parser.result.ExtraResult;

import java.util.List;

/**
 * sql路由结果总集合
 *
 * Created by liufish on 16/11/1.
 */
public class SQLRouteResult {

    /**
     * 数据库
     */
    private String schema;

    /**
     * 原有sql
     */
    private String sql;


    /**
     * sql解析而外信息
     */
    private ExtraResult extra;

    /**
     * sql解析信息
     */
    private RouteResult routeResult;

    /**
     * 路由及执行单元
     */
    private List<RouterUnit> executeUnits;


    public SQLRouteResult(String sql,String schema){
        this.sql = sql;
        this.schema = schema;
    }

    public String getSql() {
        return sql;
    }

    public String getSchema() {
        return schema;
    }

    public List<RouterUnit> getExecuteUnits() {
        return executeUnits;
    }

    public void setExecuteUnits(List<RouterUnit> executeUnits) {
        this.executeUnits = executeUnits;
    }

    public ExtraResult getExtra() {
        return extra;
    }

    public void setExtra(ExtraResult extra) {
        this.extra = extra;
    }

    public RouteResult getRouteResult() {
        return routeResult;
    }

    public void setRouteResult(RouteResult routeResult) {
        this.routeResult = routeResult;
    }

    /**
     * 路由执行单元
     */
    public static class RouterUnit{

        /**
         * 改写的sql
         */
        String routerSql;

        /**
         * 对应的db节点
         */
        DataNode node;


        public RouterUnit(String routerSql,DataNode node){

        }

        public String getRouterSql() {
            return routerSql;
        }

        public DataNode getNode() {
            return node;
        }
    }


    /**
     * 路由结果
     */
    public static class RouteResult{

        /**
         * 是否是广播表/小表/全局表,需要特殊实现
         */
        private boolean isGlobal = false;

        /**
         * 是否全部都被迫走全库,这个需要自定义注释实现
         */
        private boolean isForce = false;

        /**
         * 是否只读,强制都读库,这个需要自定义注解实现
         */
        private boolean isReadOnly = false;

        /**
         * 是否是 select 。。。for update
         */
        private boolean isForUpdate = false;

        /**
         * 是否合并结果集
         */
        private boolean isMerge;

        //function

        //group by

        //order by


    }



}
