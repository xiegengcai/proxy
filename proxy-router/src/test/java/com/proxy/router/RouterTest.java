package com.proxy.router;

import com.proxy.common.sql.SqlBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by liufish on 16/12/30.
 */
public class RouterTest {

    SQLRouteEngine routeEngine = new SQLRouteEngine();

    @Test
    public void singleTableSingleConditionTest(){
        String logicSql = "select t1.* from user t1  where t1.pid = 'abc'";
        routeEngine.route(logicSql,null);
    }

    @Test
    public void joinSelfTest(){
        String logicSql = "select t1.* from user t1 left join user t2 where t1.pid = t2.id";
        routeEngine.route(logicSql,null);
    }


    @Test
    public void singleTableMulConditionTest(){
        String logicSql = "select t1.* from user t1  where t1.pid = 'abc' and t1.name in( 1 ,2 ,3 ,4)";
        routeEngine.route(logicSql,null);
    }

    @Test
    public void selectList(){
        String logicSql = "select t1.* from user t1  where t1.pid = 'abc' and t1.name in( select va from db where id = 1)";
        routeEngine.route(logicSql,null);
    }

    @Test
    public void selectFrom(){
        String logicSql = "select t1.* from user t1 ,dep t3 where t2.k = t1.k and t1.pid = 'abc' and t1.name in( select va from db where id = 1)";
        routeEngine.route(logicSql,null);
    }

}
